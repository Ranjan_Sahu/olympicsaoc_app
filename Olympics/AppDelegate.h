//
//  AppDelegate.h
//  Olympics
//
//  Created by webwerks on 05/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EnterPinViewController.h"
#import "loginViewController.h"
#import "NoInternetAccessViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) EnterPinViewController *PinController;
@property (strong, nonatomic) loginViewController *loginController;
@property (strong, nonatomic) NoInternetAccessViewController *NoInternetController;
@property (strong, nonatomic) UINavigationController *navController;

-(void)SetLeftPaddingFor:(UITextField*)textfield width:(int)width imageIfAny:(UIImage*)image;
-(void)SetRightPaddingFor:(UITextField*)textfield width:(int)width imageIfAny:(UIImage*)image;
-(void)RemoveUserDataAndSetRootViewController;

@end

