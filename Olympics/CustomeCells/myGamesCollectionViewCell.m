//
//  myGamesCollectionViewCell.m
//  Olympics
//
//  Created by webwerks on 11/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "myGamesCollectionViewCell.h"

@implementation myGamesCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if(IS_IPAD)
    {
        self.gameTitleLabelHeight.constant = 50;
        self.imageViewWidth.constant = 240;
        //[self.lblGameTitle setFont:app_font_bold_24];
        [self.lblGameTitle setFont:app_font_bold_27];
    }
    else
    {
        self.gameTitleLabelHeight.constant = 30;
        //[self.lblGameTitle setFont:app_font_bold_13];
        [self.lblGameTitle setFont:app_font_bold_15];
        //self.imageViewWidth.constant = 114;
    }
}

@end
