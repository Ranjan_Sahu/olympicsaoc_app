//
//  FooterCustomCell.m
//  Olympics
//
//  Created by webwerks on 7/21/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "FooterCustomCell.h"

@implementation FooterCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    if(IS_IPAD){
        [_titleLabel setFont: app_font24];
    }
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
