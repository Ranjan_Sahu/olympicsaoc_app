//
//  InformationListCustomCell.m
//  Olympics
//
//  Created by webwerks on 2/24/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "InformationListCustomCell.h"

@implementation InformationListCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    if(IS_IPAD){
        [self.lblEventNameText setFont:app_font_bold_22];
        [self.lblFieldText setFont:app_font16];
        [self.DateLabel setFont:app_font_bold_16];
        [self.btnFormSubmitted.titleLabel setFont:app_font_bold_16];
        
    }
    else if(IS_IPHONE){
        [self.lblEventNameText setFont:app_font_bold_18];
        [self.lblFieldText setFont:app_font13];
        [self.DateLabel setFont:app_font_bold_13];
        [self.btnFormSubmitted.titleLabel setFont:app_font_bold_13];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
