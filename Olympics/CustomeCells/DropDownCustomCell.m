//
//  DropDownCustomCell.m
//  Olympics
//
//  Created by webwerks on 7/28/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "DropDownCustomCell.h"

@implementation DropDownCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    self.lblMsgCount.font = app_font12;
    if(IS_IPAD){
        self.widthViewMsgCount.constant = 36;
        self.heightViewMsgCount.constant = 36;
        self.lblMsgCount.font = app_font18;
    }
    
    self.viewMsgCount.layer.cornerRadius = self.widthViewMsgCount.constant/2 ;
    self.lblMsgCount.layer.cornerRadius = self.widthViewMsgCount.constant/2 ;
    self.lblMsgCount.layer.masksToBounds = TRUE;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
