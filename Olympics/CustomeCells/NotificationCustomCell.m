//
//  NotificationCustomCell.m
//  Olympics
//
//  Created by Vicky on 19/08/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "NotificationCustomCell.h"

@implementation NotificationCustomCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    if(IS_IPAD)
    {
        [_titleLabel setFont: app_font24];
        [_descriptionLabel setFont: app_font20];
    }
    else
    {
        [_titleLabel setFont: app_font16];
        [_descriptionLabel setFont: app_font13];
    }

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
