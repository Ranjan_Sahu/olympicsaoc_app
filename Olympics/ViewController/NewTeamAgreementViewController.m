//
//  NewTeamAgreementViewController.m
//  Olympics
//
//  Created by webwerks on 3/21/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "NewTeamAgreementViewController.h"
#import "TeamAgreementDetailsViewController.h"
#import "HomeScreenViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

@interface NewTeamAgreementViewController ()
{
    //UITableView *tblViewGrpQueAns;
    //AdhocCustomCell *tblCell;
    NSMutableArray *arrayAgreementList, *arrayToShowRowData, *agreementResponseData;
    NSMutableDictionary *responseDictionery;
    UIButton *agreeButton;//*pdfButton,
    //UILabel *webViewSeperator, *horizintalSeperator;
    //OLMSingleCheckBox *checkBoxOneObj;
    
    CGFloat controlHeight,rowHeight,tblViewHeight,headerHeight,headertitlelblLeading,subtractedWidth;
    NSIndexPath *selectedIndex, *previousIndex;
    NSUInteger currSection, prvSection, noOfSection;
    BOOL isSectionSelected, isSectionExpanded, isConsent, isAllSigned,isConsentData;
}

@end

@implementation NewTeamAgreementViewController
@synthesize incrementedHeight, controlls, contentViewControlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.proceedBtn.hidden=true;

    selectedIndex = [NSIndexPath indexPathForRow:0 inSection:0];
    
    isConsent = [[[NSUserDefaults standardUserDefaults] valueForKey:@"IS_CONSENT_GAME"] boolValue];
    
    [self.tapItemsCollectionView registerNib:[UINib nibWithNibName:@"agreementsCollectionViewCell" bundle:nil] forCellWithReuseIdentifier: @"agreementsCollectionViewCell"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    headerHeight = 40.0f;
    headertitlelblLeading = 15.0f;
    subtractedWidth = 25.0f;
    if(IS_IPAD)
    {
        headerHeight = 80.0f;
        headertitlelblLeading = 50.0f;
        subtractedWidth = 40.0f;
    }
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue91a"];
    self.automaticallyAdjustsScrollViewInsets = NO;

    
    // Do any additional setup after loading the view.
//    if(IS_IPAD)
//    {
//        _heightViewScrollContent.constant = 1200;
//        _heightHeaderView.constant = 260;
//        _heightViewCollection.constant = 100;
//        _heightAgreementWebView.constant = 300;
//    }
//    else if (IS_IPHONE_5)
//    {
//        _heightViewScrollContent.constant = 900;
//        _heightViewCollection.constant = 50;
//        _heightAgreementWebView.constant = 200;
//    }
    
    //[self CallServiceToGetAgreements];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self CallServiceToGetAgreements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark WebService Call
-(void)CallServiceToGetAgreements
{
    [COMMON_SETTINGS showHUD];
    isConsent = [[[NSUserDefaults standardUserDefaults] valueForKey:@"IS_CONSENT_GAME"] boolValue];
    NSDictionary *parameters = @{
                                 @"ConsentData" : @"false"
                                 };
    if(isConsent == TRUE){
        parameters = @{
                       @"ConsentData" : @"true"
                       };
        
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [ callService requestingURLString:weburl Service:Service_GetAgreementDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithArray:(NSMutableArray*)recordsArray
{
    [COMMON_SETTINGS showHUD];
    
    //NSString *deviceAddress = [NSString stringWithFormat:@"%@",[self getIPAddress]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:recordsArray forKey:@"lstSingleAgreement"];
    //[parameters setObject:deviceAddress forKey:@"IpAddress"];
    
    if(![[parameters objectForKey:@"lstSingleAgreement"]isKindOfClass:[NSNull class]] && [[parameters objectForKey:@"lstSingleAgreement"]isKindOfClass:[NSArray class]] && [[parameters objectForKey:@"lstSingleAgreement"] count]>0)
    {
        Service *callService = [[Service alloc] init];
        callService.delegate = self;
        [callService requestingURLString:weburl Service:Service_InsertUpdateTeamAgreementDetails withParameters:parameters ContactID:[_playerDetails valueForKey:@"ContactID"]];
        _serviceCalled = Service_Called_Save;
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"No records to save" CancelButtonTitle:@"OK" InView:self.view];
    }
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            isAllSigned = [[responseDictionery valueForKey:@"AllSigned"]boolValue];
            isConsentData = [[responseDictionery valueForKey:@"ConsentData"]boolValue];
            [self ShowProceedButton];
            //check value for
            if(![[responseDictionery objectForKey:@"lstSingleAgreement"] isKindOfClass:[NSNull class]])
            {
                agreementResponseData = [[NSMutableArray alloc]initWithArray:[responseDictionery objectForKey:@"lstSingleAgreement"]];
                
                arrayAgreementList = [[NSMutableArray alloc]initWithArray:[responseDictionery objectForKey:@"lstSingleAgreement"]];
                
                if(arrayAgreementList.count>0)
                {
                    NSString *heaserHtmlString = [[responseDictionery valueForKey:@"Instruction"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"Instruction"];
                    //[_headerWebView loadHTMLString:heaserHtmlString baseURL:nil];
                    
                    //[self StartAddingControllers];
                    [self.tapItemsCollectionView reloadData];
                    //[self collectionView:_tapItemsCollectionView didSelectItemAtIndexPath:selectedIndex];
                }
            }
        }
        
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma - mark additional function
-(void)ShowProceedButton{
    self.proceedBtn.hidden=true;
    if (isConsentData) {
        self.proceedBtn.hidden=false;
        [self ProceedButtonEnable];
    }
}
-(void)ProceedButtonEnable{
    if(isAllSigned)
    {
        _proceedBtn.enabled = TRUE;
        _proceedBtn.alpha = 1.0;
        
        [[NSUserDefaults standardUserDefaults] setValue:@"false" forKey:@"IS_CONSENT_GAME"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }
    else
    {
        _proceedBtn.enabled = FALSE;
        _proceedBtn.alpha = 0.7;
    }
}

#pragma - mark collectionView Deleget

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if([arrayAgreementList count]>0)
        return [arrayAgreementList count];
    else
        return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *cellIdentifier = @"agreementsCollectionViewCell";

    agreementsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    NSMutableDictionary *dict = [arrayAgreementList objectAtIndex:indexPath.row];

//    if (cell.frame.size.width >= 300)
//        cell.titleLabel.numberOfLines = 2;
//    else
//        cell.titleLabel.numberOfLines = 1;

    cell.titleLabel.text = [dict valueForKey:@"AgreementTitle"];

    [cell.titleLabel setFont:app_font13];
    if(IS_IPAD)
        [cell.titleLabel setFont:app_font24];
    
//    if(selectedIndex.row == indexPath.row )
//    {
//        [cell setBackgroundColor:[UIColor colorWithRed:0.0509 green:0.3647 blue:0.6823 alpha:1.0]];
//        [cell.titleLabel setTextColor:color_white];
//        
//        [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
//    }
//    else
//    {
        //[cell setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:83.0/255.0 blue:155.0/255.0 alpha:1.0]];
        [cell.titleLabel setTextColor:color_white];
    //}
    cell.clipsToBounds = YES;
    
    if([[dict valueForKey:@"chkAgree"] intValue] == 1)
    {
        cell.visibilityMark.hidden = FALSE;
        cell.alpha = 1.0;
    }
    else
    {
        cell.visibilityMark.hidden = TRUE;
        cell.alpha = 0.7;
    }
    return cell;
}

-(CGFloat)CalculateTitleLabelHeight:(NSInteger)indexPath
{
    CGFloat lblWidth = SCREEN_WIDTH/2 - 15;
    NSString *titleString = [[[arrayAgreementList objectAtIndex:indexPath] valueForKey:@"AgreementTitle"]stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    CGRect rFrame = [titleString boundingRectWithSize:CGSizeMake(lblWidth, CGFLOAT_MAX)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13]}
                                              context:nil];
    CGFloat lblHeight = rFrame.size.height;
    if(lblHeight<80)
        lblHeight = 80;
    else
        lblHeight = lblHeight +20;
    return lblHeight;
}
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if([arrayAgreementList count]>0)
    {
        CGFloat lblWidth = SCREEN_WIDTH/2 - 15;
        CGFloat lblHeight = [self CalculateTitleLabelHeight:indexPath.row];
        CGFloat compare = fmodf(indexPath.row, 2);
        if(compare == 0)
        {
            if (indexPath.row + 1 < [arrayAgreementList count]) {
                CGFloat tempLblHeight =  [self CalculateTitleLabelHeight:indexPath.row + 1];
                if (tempLblHeight > lblHeight) {
                    lblHeight = tempLblHeight;
                }
            }
        }
        else{
            CGFloat tempLblHeight =  [self CalculateTitleLabelHeight:indexPath.row - 1];
            if (tempLblHeight > lblHeight) {
                lblHeight = tempLblHeight;
            }
        }
        return CGSizeMake(lblWidth, lblHeight);
    }
    else
    {
        if(IS_IPAD)
            return CGSizeMake(160, 160);
        else if (IS_IPHONE)
            return CGSizeMake(160, 160);
        else
            return CGSizeMake(160, 160);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [collectionView reloadItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    
    agreementsCollectionViewCell *cell = (agreementsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    TeamAgreementDetailsViewController *hsVC=[[TeamAgreementDetailsViewController alloc]initWithNibName:@"TeamAgreementDetailsViewController" bundle:nil];
    NSDictionary *userinfo = [[NSUserDefaults standardUserDefaults] valueForKey:UD_UserInfo];
    
    hsVC.agreementHeader = [[responseDictionery valueForKey:@"Instruction"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"Instruction"];
    hsVC.agreementDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayAgreementList objectAtIndex:indexPath.row]];
    hsVC.agreementResponseDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayAgreementList objectAtIndex:indexPath.row]];
    
    hsVC.isLogedInUsersDetail = TRUE;
    hsVC.playerDetails = userinfo;
    [self.navigationController pushViewController:hsVC animated:YES];
    
}

#pragma mark - Collection View Padding Implementation
//Maintained padding between items
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - IBAction Implementation
- (IBAction)DidSelectBackButton:(id)sender {
    
    if(isConsent)
    {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                       message:@"You have not accepted all agreements, you will not be able to continue until you Read/Accept all agreements. Do you want to exit Application."
                                                                preferredStyle:(UIAlertControllerStyleAlert)];
        
        UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Continue to Agreement"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        
                                                                        [self dismissViewControllerAnimated:YES completion:^{
                                                                        }];
                                                                        
                                                                    }];
        
        UIAlertAction *alert_exit_action = [UIAlertAction actionWithTitle:@"Exit Application"
                                                                    style:(UIAlertActionStyleDestructive)
                                                                  handler:^(UIAlertAction * _Nonnull action) {
                                                                      
                                                                      exit (0);
                                                                      
                                                                  }];
        [alert addAction:alert_cancel_action];
        [alert addAction:alert_exit_action];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectProceedButton:(id)sender {
            [self.navigationController popViewControllerAnimated:YES];
}

@end
