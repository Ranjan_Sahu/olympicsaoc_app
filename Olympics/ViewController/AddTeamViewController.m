//
//  AddTeamViewControlle.m
//  Olympics
//
//  Created by webwerks2 on 7/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AddTeamViewController.h"
#import "TeamListingViewController.h"
#import "CheckBoxCustomeCell.h"


@interface AddTeamViewController ()
{
    CGFloat animatedDistance;
    NSUInteger selectedItemCount;
}

@end

@implementation AddTeamViewController
@synthesize  RecordsArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    _titleLabel.font = app_font_bold_15;
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue912"];
    selectedItemCount = 0;
    
    selectedImage = [UIImage imageNamed:@"checkbox_on"] ;
    deselectedImage = [UIImage imageNamed:@"checkbox_off"];
    isFirstTime = true;
    self.tableviewRecordList.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.tableviewRecordList.bounds.size.width, 0.01f)];
    //Create Require Fields.
    self.isChangesMade = FALSE;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    self.isChangesMade = FALSE;
    if(![RecordsArray isKindOfClass:[NSNull class]] && [RecordsArray count]>0)
    {
        if(isFirstTime)
        {
            _tableviewRecordList.dataSource = self;
            _tableviewRecordList.delegate = self;
            [_tableviewRecordList reloadData];
            isFirstTime = false;
        }
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"No Team added yet !" CancelButtonTitle:@"OK" InView:self];
    }
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if ([self.owner class]==[TeamListingViewController class]) {
        [(TeamListingViewController *)self.owner setIsNeedToRefresh:FALSE];
    }
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    if(![RecordsArray isKindOfClass:[NSNull class]] && [RecordsArray count]>0)
    {
        NSMutableArray *requestParameters = [[NSMutableArray alloc] init];
        for (int i = 0 ; i < [self.RecordsArray count]; i++)
        {
            NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
            NSInteger isselected = [[[RecordsArray objectAtIndex:i] valueForKey:@"IsSelected"] integerValue];
            if(isselected == 1)
            {
                [parameters setValue:@1 forKey:@"IsSelected"];
                [parameters setValue:[[RecordsArray objectAtIndex:i] valueForKey:@"TeamId"] forKey:@"TeamId"];
            }
            else
            {
                [parameters setValue:@0 forKey:@"IsSelected"];
                [parameters setValue:[[RecordsArray objectAtIndex:i] valueForKey:@"TeamId"] forKey:@"TeamId"];
            }
            [requestParameters addObject:parameters];
            
        }
        
        NSMutableDictionary *finalParameters = [[NSMutableDictionary alloc] init];
        [finalParameters setObject:requestParameters forKey:@"lstTeamList"];
        [self CallServiceToSaveDataWithDictionary:finalParameters];
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"No records to save" CancelButtonTitle:@"OK" InView:self];
    }
}

#pragma - mark WebService Call
-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateTeamDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
}

#pragma - mark Service Deleget

-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
    {
        if ([self.owner class]==[TeamListingViewController class]) {
            [(TeamListingViewController *)self.owner setIsNeedToRefresh:TRUE];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErroMessage"] CancelButtonTitle:@"OK" InView:self];
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}


#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return RecordsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *itemCell = @"CheckBoxCustomeCell";
    CheckBoxCustomeCell *cell = (CheckBoxCustomeCell *)[self.tableviewRecordList dequeueReusableCellWithIdentifier:itemCell];
    
    if (cell == nil)
    {
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CheckBoxCustomeCell" owner:nil options:nil];
        cell =  [nib objectAtIndex:0];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary *dict = [RecordsArray objectAtIndex:indexPath.row];
    cell.titleLabel.text = [dict valueForKey:@"Team"];
    cell.descriptionLabel.text = [dict valueForKey:@"SportDiscipline"];
    NSInteger isselected = [[dict valueForKey:@"IsSelected"] integerValue];
    cell.checkBoxButton.tag = indexPath.row;
    [cell.checkBoxButton addTarget:self action:@selector(DidSelectCheckBoxButton:) forControlEvents:UIControlEventTouchUpInside];
    if(isselected == 1 )
    {
        [cell.checkBoxButton setBackgroundImage:selectedImage forState:UIControlStateNormal];
    }
    else
    {
        [cell.checkBoxButton setBackgroundImage:deselectedImage forState:UIControlStateNormal];
        
    }
    
    return cell;
    
}

-(void)DidSelectCheckBoxButton:(UIButton*)sender
{
    UIImage *image = [sender backgroundImageForState:UIControlStateNormal];
    NSMutableDictionary *record = [[NSMutableDictionary alloc] initWithDictionary:[RecordsArray objectAtIndex:[sender tag]]];
    
    if([image isEqual:selectedImage])
    {
        [sender setBackgroundImage:deselectedImage forState:UIControlStateNormal];
        [record setValue:@0 forKey:@"IsSelected"];
    }
    else
    {
        [sender setBackgroundImage:selectedImage forState:UIControlStateNormal];
        [record setValue:@1 forKey:@"IsSelected"];
    }
    self.isChangesMade = TRUE;
    [RecordsArray replaceObjectAtIndex:[sender tag] withObject:record];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    CheckBoxCustomeCell *cell = (CheckBoxCustomeCell *)[tableView cellForRowAtIndexPath:indexPath];
    [self DidSelectCheckBoxButton:cell.checkBoxButton];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end

