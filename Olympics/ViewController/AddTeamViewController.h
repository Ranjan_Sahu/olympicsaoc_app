//
//  AddTeamViewController.h
//  Olympics
//
//  Created by webwerks2 on 7/15/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
#import "OLMSingleCheckBox.h"

@interface AddTeamViewController : UIViewController<Service_delegate,UITableViewDelegate,UITableViewDataSource>
{
    BOOL isFirstTime;
    UIImage *selectedImage;
    UIImage *deselectedImage;
}

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (nonatomic, strong) NSMutableArray *RecordsArray;
@property (nonatomic,assign) Service_Called serviceCalled;
@property (nonatomic, strong) NSDictionary *recordsDetailsDictionary;
@property (nonatomic, strong) UIViewController *owner;
@property (nonatomic, strong)  IBOutlet UITableView *tableviewRecordList;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end
