//
//  ChangePasswordViewController.h
//  Olympics
//
//  Created by webwerks on 05/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"
@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewUserPin;
@property (weak, nonatomic) IBOutlet UITextField *txtPin1;
@property (weak, nonatomic) IBOutlet UITextField *txtPin2;
@property (weak, nonatomic) IBOutlet UITextField *txtPin3;
@property (weak, nonatomic) IBOutlet UITextField *txtPin4;
@property (weak, nonatomic) IBOutlet UITextField *txtUserPin;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblEnterPin;
@property (weak, nonatomic) IBOutlet UILabel *lblPinRecieved;

- (IBAction)onClickVerify:(UIButton *)sender;
- (IBAction)DidSelectCancelButton:(id)sender;

@end

