//
//  PassportViewController.m
//  Olympics
//
//  Created by webwerks on 14/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "PassportViewController.h"
#import "OLMImagePickerView.h"
#import "UIImageView+WebCache.h"
#import "PECropViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface PassportViewController ()
{
    NSString *previousText,*currentText,*gameOpeningDateString;
    NSDate *gameOpeningDate;
    NSDictionary *imageParamsDict;
    NSMutableDictionary *requestParameters;
    UIImageView *largeImageView;
    UIImage *uploadImage;
    UIView *zoomView;
    NSString *passportScanResourceID;
    OLMImagePickerView *IMGcontrolObj;
}
@end

@implementation PassportViewController
@synthesize incrementedHeight, controlls;


- (void)viewDidLoad {
    [super viewDidLoad];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue914"];
    self.isChangesMade = FALSE;
    requestParameters = [[NSMutableDictionary alloc] init];
    [self CallServiceTogetSubMenues];
    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
        {
            OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
            if(answersDict)
            {
                if ([answersDict valueForKey:@"PassportExpiry"]) {
                    [self calculateDuration:[answersDict valueForKey:@"PassportExpiry"]];
                }
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DatePickerObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMImagePickerView class]])
        {
            IMGcontrolObj  = (OLMImagePickerView*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [IMGcontrolObj GetAnswerInfo];
            
            if(answersDict)
            {
                [imageParamsDict initWithDictionary:answersDict];
                NSLog(@"%@",imageParamsDict);
            }
            else if(IMGcontrolObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    [self callMethodToConvertIntoString:requestParameters];
}

-(void)callMethodToConvertIntoString:(NSMutableDictionary*)dict{
    
    NSMutableDictionary *tempDict = [[NSMutableDictionary alloc]init];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"CountryId"]] forKey:@"CountryId"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"CountryOfIssueId"]] forKey:@"CountryOfIssueId"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"DocumentTypeId"]] forKey:@"DocumentTypeId"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"FamilyName"]] forKey:@"FamilyName"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"GivenName"]] forKey:@"GivenName"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PassportExpiry"]] forKey:@"PassportExpiry"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PassportIssued"]] forKey:@"PassportIssued"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PassportNo"]] forKey:@"PassportNo"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PlaceOfBirth"]] forKey:@"PlaceOfBirth"];
    
    [tempDict setObject:[NSString stringWithFormat:@"%@",[dict valueForKey:@"PlaceofIssue"]] forKey:@"PlaceofIssue"];
    
    [self CallServiceToSaveDataWithDictionary:tempDict];
}

-(void)viewWillLayoutSubviews
{
    _BaseScrillViewWidth.constant = self.view.frame.size.width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma - mark WebService Call

-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters ;
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_PassportAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *imageDict = [[NSDictionary alloc] initWithObjectsAndKeys:IMGcontrolObj.imageView.image, @"filename", nil];
    [userinformation setValue:[NSString stringWithFormat:@"%@",[_DefaultValues valueForKey:@"PassportID"]] forKey:@"PassportID"];
    [userinformation setValue:passportScanResourceID forKey:@"PassportScanResourceID"];
    [userinformation setValue:@"passportImage.png" forKey:@"PassportScanFileName"];
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdatePassportDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"] imagesDict:imageDict];
    _serviceCalled = Service_Called_Save;

    
}
#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        gameOpeningDateString = [responseDictionery valueForKey:@"GameOpeningDate"];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            _DefaultValues = responseDictionery;
            _lstddlPassportCountry = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstddlPassportCountry"]];
            _lstddlCountryofIssue = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstddlCountryofIssue"]];
            _lstddlDocumentType = [[NSMutableArray alloc] initWithArray:[responseDictionery objectForKey:@"lstddlDocumentType"]];
            
            _isPassportScanVisible = [[responseDictionery valueForKey:@"PassportScanVisibility"] boolValue];
            passportScanResourceID = [NSString stringWithFormat:@"%@",[responseDictionery valueForKey:@"PassportScanResourceID"]];
            _imageURL = [responseDictionery valueForKey:@"PassportScanImage"];
            
            if(![_imageURL isKindOfClass:[NSNull class]] && ![_imageURL isEqualToString:@""])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [IMGcontrolObj.imageView sd_setImageWithURL:[NSURL URLWithString:_imageURL] placeholderImage:[UIImage imageNamed:@"photo_box"]];
                });
            }
            
            
            if([_lstddlDocumentType count] > 0 && [_lstddlCountryofIssue count] >0 && [_lstddlPassportCountry count] >0)
            {
                [self StartAddingControllers];
                self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
                [self.view bringSubviewToFront:self.navigationBarView];
            }
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErroMessage"] CancelButtonTitle:@"OK" InView:self];
        }
        
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErroMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers
{
    //Field_DropdownList
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.BaseScrollView.frame;
    
    
    //Place Text Field
    OLMTextField *PNControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Passport Number"  fieldIdName:@"PassportNo" questionId:101 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"PassportNo"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"PassportNo"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    PNControlObj.questionTypeId = Field_NumericTextField;
    PNControlObj.tag = 0;
    [controlls addObject:PNControlObj];
    [self.BaseScrollView addSubview:PNControlObj];
    incrementedHeight = incrementedHeight + PNControlObj.frame.size.height + 5;
    
    //City Text Field
    OLMTextField *NAMEControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Given Name(s) as per Passport"  fieldIdName:@"GivenName" questionId:102 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"GivenName"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"GivenName"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    NAMEControlObj.questionTypeId = Field_TextField;
    NAMEControlObj.tag = 1;
    [controlls addObject:NAMEControlObj];
    [self.BaseScrollView addSubview:NAMEControlObj];
    incrementedHeight = incrementedHeight + NAMEControlObj.frame.size.height + 5;
    
    //City Text Field
    OLMTextField *FNameControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Family Name as per Passport"  fieldIdName:@"FamilyName" questionId:103 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"FamilyName"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"FamilyName"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    FNameControlObj.questionTypeId = Field_TextField;
    FNameControlObj.tag = 2;
    [controlls addObject:FNameControlObj];
    [self.BaseScrollView addSubview:FNameControlObj];
    incrementedHeight = incrementedHeight + FNameControlObj.frame.size.height + 5;
    
    
    if(![_lstddlPassportCountry isKindOfClass:[NSNull class]] && _lstddlPassportCountry && [_lstddlPassportCountry count]>0)
    {
        OLMDropDown *StateControlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Country as per Passport"  fieldIdName:@"CountryId" questionId:104 Options:_lstddlPassportCountry IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"CountryId"] intValue] delegate:self];
        StateControlObj.questionTypeId = Field_DropdownList;
        StateControlObj.tag = 3;
        [controlls addObject:StateControlObj];
        [self.BaseScrollView addSubview:StateControlObj];
        incrementedHeight = incrementedHeight + StateControlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    //City Text Field
    OLMTextField *ZipControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Place of Issue"  fieldIdName:@"PlaceofIssue" questionId:105 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"PlaceofIssue"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"PlaceofIssue"] keyboardType:UIKeyboardTypeDefault delegate:self];
    
    ZipControlObj.questionTypeId = Field_TextField;
    ZipControlObj.tag = 4;
    [controlls addObject:ZipControlObj];
    [self.BaseScrollView addSubview:ZipControlObj];
    incrementedHeight = incrementedHeight + ZipControlObj.frame.size.height + 5;
    
    if(![_lstddlCountryofIssue isKindOfClass:[NSNull class]] && _lstddlCountryofIssue && [_lstddlCountryofIssue count]>0)
    {
        OLMDropDown *CountryControlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Country of Issue"  fieldIdName:@"CountryOfIssueId" questionId:106 Options:_lstddlCountryofIssue IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"CountryOfIssueId"] intValue] delegate:self];
        CountryControlObj.questionTypeId = Field_DropdownList;
        CountryControlObj.tag = 5;
        [controlls addObject:CountryControlObj];
        [self.BaseScrollView addSubview:CountryControlObj];
        incrementedHeight = incrementedHeight + CountryControlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    //City Text Field
    OLMTextField *POBControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Place of Birth"  fieldIdName:@"PlaceOfBirth" questionId:107 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"PlaceOfBirth"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"PlaceOfBirth"] keyboardType:UIKeyboardTypeDefault delegate:self];
    
    POBControlObj.questionTypeId = Field_TextField;
    POBControlObj.tag = 6;
    [controlls addObject:POBControlObj];
    [self.BaseScrollView addSubview:POBControlObj];
    incrementedHeight = incrementedHeight + POBControlObj.frame.size.height + 5;
    
    OLMDatePicker *PIcontrolObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Passport Issued"  fieldIdName:@"PassportIssued" questionId:108 IsRequired:NO defaultValue:[_DefaultValues valueForKey:@"PassportIssued"] delegate:self];
    PIcontrolObj.questionTypeId = Field_DatePicker;
    PIcontrolObj.tag = 7;
    [controlls addObject:PIcontrolObj];
    [self.BaseScrollView addSubview:PIcontrolObj];
    incrementedHeight = incrementedHeight + PIcontrolObj.frame.size.height + 5;
    
    OLMDatePicker *PEcontrolObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Passport Expiry"  fieldIdName:@"PassportExpiry" questionId:109 IsRequired:NO defaultValue:[_DefaultValues valueForKey:@"PassportExpiry"]  delegate:self];
    PEcontrolObj.questionTypeId = Field_DatePicker;
    PEcontrolObj.tag = 8;
    [controlls addObject:PEcontrolObj];
    [self.BaseScrollView addSubview:PEcontrolObj];
    incrementedHeight = incrementedHeight + PEcontrolObj.frame.size.height + 5;
    
    if(![_lstddlDocumentType isKindOfClass:[NSNull class]] && _lstddlDocumentType && [_lstddlDocumentType count]>0)
    {
        OLMDropDown *DTControlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Document Type"  fieldIdName:@"DocumentTypeId" questionId:110 Options:_lstddlDocumentType IsRequired:YES defaultValue:[[_DefaultValues valueForKey:@"DocumentTypeId"] intValue] delegate:self];
        DTControlObj.questionTypeId = Field_DropdownList;
        DTControlObj.tag = 9;
        [controlls addObject:DTControlObj];
        [self.BaseScrollView addSubview:DTControlObj];
        incrementedHeight = incrementedHeight + DTControlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
        //Field_ImagePickerView
        IMGcontrolObj = [[OLMImagePickerView alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Select Passport Scaned Image"  fieldIdName:@"Select Passport Scaned Image" questionId:111 IsRequired:YES delegate:self];
        IMGcontrolObj.questionTypeId = Field_ImagePickerView;
        IMGcontrolObj.tag = 10;
        [controlls addObject:IMGcontrolObj];
        [self.BaseScrollView addSubview:IMGcontrolObj];
        incrementedHeight = incrementedHeight + IMGcontrolObj.frame.size.height + 5;
    
        self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"imagePassport.png"]; //Add the file name
    NSData *pngData = [NSData dataWithContentsOfFile:filePath];
    UIImage *image1 = [UIImage imageWithData:pngData];
    IMGcontrolObj.imageView.image = image1;

    
    self.isChangesMade = FALSE;
}
#pragma mark - OLMImagePickerDelegate methods
-(void)OLMImagePickerDidDismissWithImage:(UIImage*) image forImageView:(UIImageView*)imageView
{
    //    uploadImage = [[UIImage alloc] init];
    //    uploadImage = imageView.image;
    [self openEditor:image];
    
}

- (void)openEditor:(UIImage*) image1
{
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = (id)self;
    controller.image = image1;
    
    UIImage *image = image1;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2,
                                          (height - length) / 2,
                                          length * 0.78,
                                          length);
    //controller.cropAspectRatio = 6.0F / 10.0F;
    controller.keepingCropAspectRatio = YES;
    
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        navigationController.modalPresentationStyle = UIModalPresentationFormSheet;
    }
    
    [self presentViewController:navigationController animated:YES completion:NULL];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return largeImageView;
}

-(void)DidSelectCloseZoomView:(UIButton*)sender
{
    for (UIView *view in zoomView.subviews) {
        [view removeFromSuperview];
    }
    [zoomView removeFromSuperview];
}

#pragma mark - PECropViewControllerDelegate methods

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect
{
    [controller dismissViewControllerAnimated:YES completion:NULL];
    
    NSData *pngData = UIImagePNGRepresentation(croppedImage);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *filePath = [documentsPath stringByAppendingPathComponent:@"imagePassport.png"]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    //uploadImage = croppedImage;
    IMGcontrolObj.imageView.image = croppedImage;
    //    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
    //        IMGcontrolObj.imageView.image = croppedImage;
    //    }
}

- (void)cropViewControllerDidCancel:(PECropViewController *)controller
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // [self updateEditButtonEnabled];
    }
    
    [controller dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText =textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText =textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText =textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText =textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDatePicker Delegate

-(void)OLMDatepickerDidStartEditing:(UITextField*)textField
{
    previousText =textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}

-(void)OLMDatepickerDidEndEditing:(UITextField*)textField
{
    currentText =textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.BaseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.BaseScrollView.window convertRect:self.BaseScrollView.bounds fromView:self.BaseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Format date to show Relative Time Duration
-(NSString *)calculateDuration:(NSString *)expiryDate
{
    // Format expiry date
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setFormatterBehavior:NSDateFormatterBehavior10_4];
    [df setDateFormat:@"dd/MM/yyyy"];
    [df setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *convertedDate = [df dateFromString:expiryDate];
    
    // Formate game opening date
    NSString *gameOpenDateString = gameOpeningDateString;
    NSDateFormatter *df1 = [[NSDateFormatter alloc] init];
    [df1 setDateFormat:@"dd/MM/yyyy"];
    [df1 setTimeZone:[NSTimeZone localTimeZone]];
    NSDate *gameOpenDate = [df dateFromString:gameOpenDateString];
    [self lastDayOfMonthDateForDate:gameOpenDate];
    
    double ti = [convertedDate timeIntervalSinceDate:gameOpeningDate];
    ti = ti * -1;
    int numberOfDays = ti / 86400;
    NSLog(@"%d",numberOfDays);
    
    if (numberOfDays>0 && numberOfDays<182) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Your passport will expire within 6 months of the opening ceremony."preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                   {
                                       [self callMethodToConvertIntoString:requestParameters];
                                   }];
        [alert addAction:okAction];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    return[NSString stringWithFormat:@"%d Days ago", numberOfDays];
}

- (NSDate *)lastDayOfMonthDateForDate:(NSDate *)date {
    NSDateComponents *comp = [NSDateComponents new];
    comp.day = -1;
    comp.month = 6;
    gameOpeningDate = [[NSCalendar currentCalendar] dateByAddingComponents:comp toDate:date options:0];
    return gameOpeningDate;
}
@end
