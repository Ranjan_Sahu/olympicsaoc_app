//
//  ContactListngViewController.h
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSwipeTableCell.h"
#import "AddContactViewController.h"
#import "TestData.h"
#import "MGSwipeButton.h"
#import "addressListTableViewCell.h"
#import "FooterCustomCell.h"

@interface ContactListngViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,MGSwipeTableCellDelegate, Service_delegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (strong, nonatomic) IBOutlet UITableView *tableviewContactlist;
@property(nonatomic,assign) Service_Called serviceCalled;
@property (nonatomic, assign) BOOL isNeedToRefresh;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;

- (IBAction)DidSelectBackButton:(id)sender;


@end
