//
//  DisabilityViewController.m
//  Olympics
//
//  Created by webwerks on 14/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "DisabilityViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface DisabilityViewController ()
{
    NSMutableArray *disabiltyTypeArray;
    NSMutableDictionary *disabilityDetails;
    NSString *previousText,*currentText;
}
@end

@implementation DisabilityViewController
@synthesize incrementedHeight, controlls;


- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirstTime = true;
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue905"];
    self.isChangesMade = FALSE;
    
    [self CallServiceTogetSubMenues];
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_isFirstTime)
    {
        //[self StartAddingControllers];
        _isFirstTime = false;
    }
}

#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc]init];//= @{ @"IsMyTeamMember": @"", @"OrgID": @"" };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetDisabilityAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateDisabilityDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Save;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            //NSLog(@"%@",responseDictionery);
            
            disabiltyTypeArray = [responseDictionery valueForKey:@"lstDisabilityType"];
            disabilityDetails = responseDictionery;
            
            if(disabilityDetails)
            {
                [self StartAddingControllers];
                self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
                [self.view bringSubviewToFront:self.navigationBarView];
            }
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
        
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - IBAction Implementation
- (IBAction)DidSelectBackButton:(id)sender {
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

-(void)viewWillLayoutSubviews
{
    _BaseScrillViewWidth.constant = self.view.frame.size.width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers
{
    //Field_DropdownList
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.BaseScrollView.frame;
    
    //OLM DropDown
    if(![disabiltyTypeArray isKindOfClass:[NSNull class]] && disabiltyTypeArray && [disabiltyTypeArray count]>0)
    {
        NSInteger initialIndex = 0;
        if(![[disabilityDetails valueForKey:@"DisabilityTypeId"]isKindOfClass:[NSNull class]])
        {
            
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"Key == %@",disabilityDetails[@"DisabilityTypeId"]];
            
            NSArray *filteredValues = [disabiltyTypeArray filteredArrayUsingPredicate:filterPredicate];
            
            if(filteredValues.count){
                initialIndex = [[filteredValues lastObject][@"Key"]integerValue];
            }
        }
        
        OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Disability Type" fieldIdName:@"DisabilityTypeId"  questionId:101 Options:disabiltyTypeArray IsRequired:NO defaultValue:initialIndex delegate:self];
        controlObj.questionTypeId = Field_DropdownList;
        controlObj.tag = 0;
        [controlls addObject:controlObj];
        [self.BaseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    //OLM CheckBox
    OLMSingleCheckBox *checkBoxOneObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:102 fieldIdName:@"chkWheelChair" OptionTitle:@"Daily Wheel Chair User." isRequired:NO defaultSelected:[disabilityDetails[@"chkWheelChair"] boolValue] delegate:self];
    checkBoxOneObj.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj.tag = 1;
    [controlls addObject:checkBoxOneObj];
    [self.BaseScrollView addSubview:checkBoxOneObj];
    incrementedHeight = incrementedHeight + checkBoxOneObj.frame.size.height + 5;
    
    //OLM CheckBox
    OLMSingleCheckBox *checkBoxTwoObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:103 fieldIdName:@"chkGuideDog" OptionTitle:@"Guide Dog" isRequired:NO defaultSelected:[disabilityDetails[@"chkGuideDog"] boolValue] delegate:self];
    checkBoxTwoObj.questionTypeId = Field_SingleCheckBox;
    checkBoxTwoObj.tag = 2;
    [controlls addObject:checkBoxTwoObj];
    [self.BaseScrollView addSubview:checkBoxTwoObj];
    incrementedHeight = incrementedHeight + checkBoxTwoObj.frame.size.height + 5;
    
    self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
    
    self.isChangesMade = FALSE;
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMSingleCheckBox:(OLMSingleCheckBox *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}
#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade =TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.BaseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.BaseScrollView.window convertRect:self.BaseScrollView.bounds fromView:self.BaseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
