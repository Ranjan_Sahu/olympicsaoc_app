//
//  MyGamesViewController.h
//  Olympics
//
//  Created by webwerks on 11/14/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGamesViewController : UIViewController <Service_delegate, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource>
{
    NSCache *imageCache;
}
@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIImageView *imageUserPic;
@property (weak, nonatomic) IBOutlet UIButton *btnDropDown;
@property (weak, nonatomic) IBOutlet UICollectionView *gamesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *viewGamesCollectionView;
@property (weak, nonatomic) IBOutlet UIView *dropDownView;
@property (weak, nonatomic) IBOutlet UITableView *dropDownTableView;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownBgImage;

@property(nonatomic, assign) Service_Called serviceCalled;
@property (assign, nonatomic) NSString *messageCount;
@property (nonatomic, assign) BOOL isNeedToRefresh;
@property (nonatomic, assign) BOOL commonWealthStatusValue;

- (IBAction)onClickDropDown:(id)sender;

@end
