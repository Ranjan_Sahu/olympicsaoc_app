//
//  DisabilityViewController.h
//  Olympics
//
//  Created by webwerks on 14/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMDropDown.h"
#import "OLMSingleCheckBox.h"
#import "OLMTextField.h"

@interface ResidenceViewController : UIViewController <Service_delegate>

@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (weak, nonatomic) IBOutlet UIScrollView *BaseScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BaseScrillViewWidth;
@property(nonatomic, assign) Service_Called serviceCalled;

@property(nonatomic, strong) NSMutableArray *stateList;
@property(nonatomic, strong) NSMutableArray *countryList;
@property(nonatomic, strong) NSMutableDictionary *DefaultValues;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end
