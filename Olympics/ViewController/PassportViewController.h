//
//  PassportViewController.h
//  Olympics
//
//  Created by Manisha Roy on 15/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OLMDropDown.h"
#import "OLMSingleCheckBox.h"
#import "OLMTextField.h"
#import "OLMDatePicker.h"
#import "OLMImagePickerView.h"


@interface PassportViewController : UIViewController<Service_delegate>


@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *navigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogo;
@property (weak, nonatomic) IBOutlet UIScrollView *BaseScrollView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *BaseScrillViewWidth;
@property(nonatomic, assign) Service_Called serviceCalled;

@property(nonatomic, strong) NSMutableArray *lstddlCountryofIssue;
@property(nonatomic, strong) NSMutableArray *lstddlPassportCountry;
@property(nonatomic, strong) NSMutableArray *lstddlDocumentType;
@property(nonatomic, strong) NSMutableDictionary *DefaultValues;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;
@property (nonatomic, strong) NSString *imageURL;
@property (assign, nonatomic) BOOL isFirstTime;
@property (assign,nonatomic) BOOL isPassportScanVisible;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;

@end
