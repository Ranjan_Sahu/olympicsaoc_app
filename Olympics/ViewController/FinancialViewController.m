//
//  FinancialViewController.m
//  Olympics
//
//  Created by webwerks on 14/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "FinancialViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface FinancialViewController ()
{
    NSString *previousText,*currentText;
}
@end

@implementation FinancialViewController
@synthesize incrementedHeight, controlls;


- (void)viewDidLoad {
    [super viewDidLoad];
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue908"];
    self.isChangesMade = FALSE;
    
    [self CallServiceTogetSubMenues];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

- (IBAction)DidSelectBackButton:(id)sender {
    if(self.isChangesMade)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

-(void)viewWillLayoutSubviews
{
    _BaseScrillViewWidth.constant = self.view.frame.size.width;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers
{
    //Field_DropdownList
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.BaseScrollView.frame;
    
    //PN Text Field
    OLMTextField *InstitutionControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Institution Name"  fieldIdName:@"InstitutionName" questionId:101 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"InstitutionName"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"InstitutionName"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    InstitutionControlObj.questionTypeId = Field_NumericTextField;
    InstitutionControlObj.tag = 0;
    [controlls addObject:InstitutionControlObj];
    [self.BaseScrollView addSubview:InstitutionControlObj];
    incrementedHeight = incrementedHeight + InstitutionControlObj.frame.size.height + 5;
    
    //Name Text Field
    OLMTextField *BSBControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"BSB No."  fieldIdName:@"BsbNo" questionId:102 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"BsbNo"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"BsbNo"] keyboardType:UIKeyboardTypeNumberPad  delegate:self];
    
    BSBControlObj.questionTypeId = Field_TextField;
    BSBControlObj.tag = 1;
    [controlls addObject:BSBControlObj];
    [self.BaseScrollView addSubview:BSBControlObj];
    incrementedHeight = incrementedHeight + BSBControlObj.frame.size.height + 5;
    
    //Swift Text Field
    OLMTextField *SwiftControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Swift Code"  fieldIdName:@"SwiftCode" questionId:103 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"SwiftCode"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"SwiftCode"] keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    SwiftControlObj.questionTypeId = Field_TextField;
    SwiftControlObj.tag = 2;
    [controlls addObject:SwiftControlObj];
    [self.BaseScrollView addSubview:SwiftControlObj];
    incrementedHeight = incrementedHeight + SwiftControlObj.frame.size.height + 5;
    
    
    //accoun no Text Field
    OLMTextField *AccNoControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Account No"  fieldIdName:@"AccountNo" questionId:104 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"AccountNo"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"AccountNo"] keyboardType:UIKeyboardTypeNumberPad delegate:self];
    
    AccNoControlObj.questionTypeId = Field_NumericTextField;
    AccNoControlObj.tag = 3;
    [controlls addObject:AccNoControlObj];
    [self.BaseScrollView addSubview:AccNoControlObj];
    incrementedHeight = incrementedHeight + AccNoControlObj.frame.size.height + 5;
    
    //account name Text Field
    OLMTextField *AccNameControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Account Name"  fieldIdName:@"AccountName" questionId:105 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"AccountName"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"AccountName"] keyboardType:UIKeyboardTypeDefault delegate:self];
    
    AccNameControlObj.questionTypeId = Field_TextField;
    AccNameControlObj.tag = 4;
    [controlls addObject:AccNameControlObj];
    [self.BaseScrollView addSubview:AccNameControlObj];
    incrementedHeight = incrementedHeight + AccNameControlObj.frame.size.height + 5;
    
    //ABS Text Field
    OLMTextField *ABNControlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"ABN"  fieldIdName:@"ABN" questionId:106 IsRequired:NO defaultValue:[[_DefaultValues valueForKey:@"ABN"] isKindOfClass:[NSNull class]]?@"":[_DefaultValues valueForKey:@"ABN"] keyboardType:UIKeyboardTypeDefault delegate:self];
    
    ABNControlObj.questionTypeId = Field_TextField;
    ABNControlObj.tag = 5;
    [controlls addObject:ABNControlObj];
    [self.BaseScrollView addSubview:ABNControlObj];
    incrementedHeight = incrementedHeight + ABNControlObj.frame.size.height + 5;
    
    self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
    
    self.isChangesMade = FALSE;
}

#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters ;
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_FinancialAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateFinancialDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Save;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            _DefaultValues = responseDictionery;
            [self StartAddingControllers];
            self.BaseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
            [self.view bringSubviewToFront:self.navigationBarView];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErroMessage"] CancelButtonTitle:@"OK" InView:self];
        }
        
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErroMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDatePicker Delegate

-(void)OLMDatepickerDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}

-(void)OLMDatepickerDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.BaseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.BaseScrollView.window convertRect:self.BaseScrollView.bounds fromView:self.BaseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.BaseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.BaseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
