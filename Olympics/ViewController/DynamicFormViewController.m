//
//  DynamicFormViewController.m
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "DynamicFormViewController.h"

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;

@interface DynamicFormViewController ()

{
    NSString *previousText,*currentText;
}
@end

@implementation DynamicFormViewController
@synthesize incrementedHeight, controlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    _navigatiobBarLogi.text = [NSString stringWithFormat:@"\ue915"];
    self.isChangesMade = FALSE;
    // DO OtherProcessing
    [self CallServiceTogetSubMenues];
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}
-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMRadioButton class]])
        {
            OLMRadioButton *RadioButtonObj  = (OLMRadioButton*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [RadioButtonObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(RadioButtonObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [RadioButtonObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMCheckBox class]])
        {
            OLMCheckBox *CheckBoxObj  = (OLMCheckBox*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMRadioButtonTextKeyType class]])
        {
            OLMRadioButtonTextKeyType *CheckBoxObj  = (OLMRadioButtonTextKeyType*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                if([[answersDict valueForKey:@"Handed"] integerValue] == 1)
                    [requestParameters setObject:@"" forKey:@"Handed"];
                else if([[answersDict valueForKey:@"Handed"] integerValue] == 2)
                    [requestParameters setObject:@"L" forKey:@"Handed"];
                else if([[answersDict valueForKey:@"Handed"] integerValue] == 3)
                    [requestParameters setObject:@"R" forKey:@"Handed"];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
        {
            OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DatePickerObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    [self  CallServiceToSaveDataWithDictionary:requestParameters];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
}


#pragma - mark WebService Call
-(void)CallServiceTogetSubMenues
{
    [COMMON_SETTINGS showHUD];
    
    NSMutableDictionary *parameters  = [[NSMutableDictionary alloc] init];
    if(_isLogedInUsersDetail)
    {
        [parameters setValue:@false forKey:@"IsMyTeamMember"];
    }
    else
    {
        [parameters setValue:@true forKey:@"IsMyTeamMember"];
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_PersonalDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
}

-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdatePersonalAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Save;
}


#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    
    if(_serviceCalled == Service_Called_GetDetail)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            self.fieldsToShowArray = [responseDictionery objectForKey:@"lstPersoanlDetails"];
            if([self.fieldsToShowArray count] > 0)
            {
                [self StartAddingControllers];
                self.baseScrollView.contentSize = CGSizeMake(320, incrementedHeight + 20 );
                [self.view bringSubviewToFront:self.vanigationBarView];
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]boolValue])
        {
            if(_isLogedInUsersDetail)
            {
                NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:UD_UserInfo]];
                NSString *fullName = [NSString stringWithFormat:@"%@ %@",[responseDictionery valueForKey:@"FirstName"],[responseDictionery valueForKey:@"LastName"]];
                [userInfo setValue:fullName forKey:@"FullName"];
                [[NSUserDefaults standardUserDefaults] setObject:userInfo forKey:UD_UserInfo];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            else
            {
                NSData *data = [[[NSUserDefaults standardUserDefaults] valueForKey:UD_ArrayMyTeamListString] dataUsingEncoding:NSUTF8StringEncoding];
                NSMutableArray *arrayTeamList = [NSMutableArray arrayWithArray:[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error]];
                
                NSMutableDictionary *dictCurrentPlayer = [NSMutableDictionary dictionaryWithDictionary:[arrayTeamList objectAtIndex:[[[NSUserDefaults standardUserDefaults] valueForKey:UD_MyTeamListCurrentIndex] integerValue]]];
                
                if(![dictCurrentPlayer isKindOfClass:[NSNull class]] && dictCurrentPlayer && [dictCurrentPlayer count]>0)
                {
                    [dictCurrentPlayer setValue:[responseDictionery valueForKey:@"FirstName"] forKey:@"FirstName"];
                    [dictCurrentPlayer setValue:[responseDictionery valueForKey:@"LastName"] forKey:@"LastName"];
                    
                    NSString *name = [NSString stringWithFormat:@"%@ %@",[responseDictionery valueForKey:@"FirstName"],[responseDictionery valueForKey:@"LastName"]];
                    [dictCurrentPlayer setValue:name forKey:@"Name"];
                    
                    [arrayTeamList replaceObjectAtIndex:[[[NSUserDefaults standardUserDefaults] valueForKey:UD_MyTeamListCurrentIndex] integerValue] withObject:dictCurrentPlayer];
                    
                    NSData *arrayMyTeamListjsonData = [NSJSONSerialization dataWithJSONObject:arrayTeamList
                                                                                      options:NSJSONWritingPrettyPrinted
                                                                                        error:&error];
                    
                    NSString *arrayMyTeamListString = [[NSString alloc] initWithData:arrayMyTeamListjsonData encoding:NSUTF8StringEncoding];
                    [[NSUserDefaults standardUserDefaults] setObject:arrayMyTeamListString forKey:UD_ArrayMyTeamListString];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - Method to add questions on screen

-(void)StartAddingControllers {
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    
    for (int i = 0 ; i < [self.fieldsToShowArray count]; i++)
    {
        NSDictionary *QDictionery = [self.fieldsToShowArray objectAtIndex:i];
        int controlTypeId = [[QDictionery valueForKey:@"ControlTypeId"] intValue];
        switch (controlTypeId) {
            case Field_TextField:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //Field_TextField
                OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]  fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] IsRequired:isRequired defaultValue:[[QDictionery valueForKey:@"textData"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"textData"] keyboardType:UIKeyboardTypeDefault  delegate:self];
                
                controlObj.questionTypeId = Field_TextField;
                controlObj.tag = i;
                [controlls addObject:controlObj];
                
                if([[QDictionery valueForKey:@"Visibility"] boolValue])
                {
                    [self.baseScrollView addSubview:controlObj];
                    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                }
            }
                break;
            case Field_DropdownList:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //Field_DropdownList
                NSArray *optionsArr = [QDictionery valueForKey:@"DropdownData"];
                if(![optionsArr isKindOfClass:[NSNull class]] && optionsArr && [optionsArr count]>0)
                {
                    OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]  fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] Options:optionsArr IsRequired:isRequired defaultValue:[[QDictionery valueForKey:@"SelectedItemValue"] intValue] delegate:self];
                    controlObj.questionTypeId = Field_DropdownList;
                    controlObj.tag = i;
                    [controlls addObject:controlObj];
                    
                    if([[QDictionery valueForKey:@"Visibility"] boolValue])
                    {
                        [self.baseScrollView addSubview:controlObj];
                        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                    }
                }
                else
                {
                    //NSLog(@"Option Array Not Found");
                }
            }
                break;
            case Field_RadioButton:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //Field_RadioButton
                NSArray *optionsArr = [QDictionery valueForKey:@"DropdownData"];
                if(![optionsArr isKindOfClass:[NSNull class]] && optionsArr && [optionsArr count]>0)
                {
                    OLMRadioButton *controlObj = [[OLMRadioButton alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]  fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] options:optionsArr IsRequired:isRequired defaultValue:[[QDictionery valueForKey:@"SelectedItemValue"] intValue] delegate:self];
                    controlObj.questionTypeId = Field_RadioButton;
                    controlObj.tag = i;
                    [controlls addObject:controlObj];
                    
                    if([[QDictionery valueForKey:@"Visibility"] boolValue])
                    {
                        [self.baseScrollView addSubview:controlObj];
                        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                    }
                }
                else
                {
                    //NSLog(@"Option Array Not Found");
                }
                
            }
                break;
            case Field_TextKeyValueList:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //
                NSMutableArray *optionsArrToEdit = [[NSMutableArray alloc]initWithArray:[QDictionery valueForKey:@"strRadioItem"]];
                for(int i=0;i<[optionsArrToEdit count];i++)
                {
                    NSMutableDictionary *dictToReplace = [[NSMutableDictionary alloc]initWithDictionary:[optionsArrToEdit objectAtIndex:i]];
                    
                    if([[dictToReplace valueForKey:@"strKey"] isEqualToString:@""])
                        [dictToReplace setValue:@"1" forKey:@"strKey"];
                    else if([[dictToReplace valueForKey:@"strKey"] isEqualToString:@"L"])
                        [dictToReplace setValue:@"2" forKey:@"strKey"];
                    else if ([[dictToReplace valueForKey:@"strKey"] isEqualToString:@"R"])
                        [dictToReplace setValue:@"3" forKey:@"strKey"];
                    
                    [optionsArrToEdit replaceObjectAtIndex:i withObject:dictToReplace];
                }
                
                NSArray *optionsArr = [[NSArray alloc]initWithArray:optionsArrToEdit];
                NSUInteger defaultValue = 0;
                
                if([[QDictionery valueForKey:@"HandedSelected"] isEqualToString:@""])
                    defaultValue = 1;
                else if([[QDictionery valueForKey:@"HandedSelected"] isEqualToString:@"L"])
                    defaultValue = 2;
                else if([[QDictionery valueForKey:@"HandedSelected"] isEqualToString:@"R"])
                    defaultValue = 3;
                //
                
                if(![optionsArr isKindOfClass:[NSNull class]] && optionsArr && [optionsArr count]>0)
                {
                    OLMRadioButtonTextKeyType *controlObj = [[OLMRadioButtonTextKeyType alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]  fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] options:optionsArr IsRequired:isRequired defaultValue:defaultValue delegate:self];
                    controlObj.questionTypeId = Field_TextKeyValueList;
                    controlObj.tag = i;
                    [controlls addObject:controlObj];
                    
                    if([[QDictionery valueForKey:@"Visibility"] boolValue])
                    {
                        [self.baseScrollView addSubview:controlObj];
                        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                    }
                }
                else
                {
                    //NSLog(@"Option Array Not Found");
                }
            }
                break;
            case Field_DatePicker:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //Field_DatePicker
                OLMDatePicker *controlObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]   fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] IsRequired:isRequired defaultValue:[QDictionery valueForKey:@"textData"]  delegate:self];
                controlObj.questionTypeId = Field_DatePicker;
                controlObj.tag = i;
                [controlls addObject:controlObj];
                
                if([[QDictionery valueForKey:@"Visibility"] boolValue])
                {
                    [self.baseScrollView addSubview:controlObj];
                    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                }
            }
                break;
            case Field_CheckBox:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //Field_CheckBox
                NSArray *optionsArr = [QDictionery valueForKey:@"DropdownData"];
                if(![optionsArr isKindOfClass:[NSNull class]] && optionsArr && [optionsArr count]>0)
                {
                    OLMCheckBox *controlObj = [[OLMCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]  fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] options:optionsArr IsRequired:isRequired defaultValue:[[QDictionery valueForKey:@"IsRequired"] intValue] delegate:self];
                    controlObj.questionTypeId = Field_CheckBox;
                    controlObj.tag = i;
                    [controlls addObject:controlObj];
                    
                    if([[QDictionery valueForKey:@"Visibility"] boolValue])
                    {
                        [self.baseScrollView addSubview:controlObj];
                        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                    }
                }
                else
                {
                    //NSLog(@"Option Array Not Found");
                }
            }
                break;
            case Field_NumericTextField:
            {
                int isRequired = [[QDictionery valueForKey:@"IsRequired"] intValue];
                if([[QDictionery valueForKey:@"Visibility"] intValue] == 0)
                {
                    isRequired = 0;
                }
                
                //Field_NumericTextField
                OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:[QDictionery valueForKey:@"lblText"]  fieldIdName:[QDictionery valueForKey:@"FieldIdName"] questionId:[[QDictionery valueForKey:@"FieldSectionId"] intValue] IsRequired:isRequired defaultValue:[QDictionery valueForKey:@"textData"] keyboardType:UIKeyboardTypeDecimalPad  delegate:self];
                
                controlObj.questionTypeId = Field_NumericTextField;
                controlObj.tag = i;
                [controlls addObject:controlObj];
                
                if([[QDictionery valueForKey:@"Visibility"] boolValue])
                {
                    [self.baseScrollView addSubview:controlObj];
                    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
                }
            }
                break;
            case Field_TextLabel:
            {
                //Field_TextLabel
                NSString *message =  [QDictionery valueForKey:@"lblText"];
                OLMLable *controlObj = [[OLMLable alloc] initWithFrame:CGRectMake(20, incrementedHeight, frame.size.width-40, 20) Text:message];
                
                controlObj.questionTypeId = Field_TextLabel;
                controlObj.tag = i;
                [controlls addObject:controlObj];
                
                if([[QDictionery valueForKey:@"Visibility"] boolValue])
                {
                    [self.baseScrollView addSubview:controlObj];
                    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5 ;
                }
            }
                break;
                
            default:
                break;
        }
    }
    incrementedHeight = incrementedHeight +30;
    self.isChangesMade = FALSE;
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMRadioButton:(OLMRadioButton *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
    
}

#pragma mark - OLMCheckBox Delegate
-(void)OLMCheckBox:(OLMCheckBox *)checkBox didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
    
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

-(void)OLMRadioButtonTextKeyType:(OLMRadioButtonTextKeyType*)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}

#pragma mark - OLMDatePicker Delegate
-(void)OLMDatepickerDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}

-(void)OLMDatepickerDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
    {
        [self CallMethodToSaveData];
    }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];

}
@end
