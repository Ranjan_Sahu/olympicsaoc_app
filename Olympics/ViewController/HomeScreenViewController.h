//
//  HomeScreenViewController.h
//  Olympics
//
//  Created by webwerks on 07/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PersonalDetailViewController.h"
#import "EnterPinViewController.h"
#import "ChangePasswordViewController.h"
#import "MyTeamListingViewController.h"
#import "NotificationListingViewController.h"
#import "InformationListViewController.h"
#import "TeamAgreementsViewController.h"

@interface HomeScreenViewController : UIViewController< UITableViewDelegate, UITableViewDataSource, Service_delegate >

@property (weak, nonatomic) IBOutlet UIImageView *imgUserPic;
@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property(nonatomic, assign) Service_Called serviceCalled;
@property (weak, nonatomic) IBOutlet UIImageView *gameImage;

@property (nonatomic, assign) BOOL commonWealthStatus;

- (void)onClickMyTeamBtn:(UIButton *)sender;
- (void)onClickInformationBtn:(UIButton *)sender;
- (void)onClickAgreementBtn:(UIButton *)sender;
- (void)onClickPersonalInfoBtn:(UIButton *)sender;
- (IBAction)onClickDropDownBtn:(UIButton *)sender;

@property (assign, nonatomic) NSString *messageCount;
@property (nonatomic, assign) BOOL isNeedToRefresh;

// Drop Down View outlets
@property (weak, nonatomic) IBOutlet UIView *dropDownView;
@property (weak, nonatomic) IBOutlet UITableView *dropDownTblView;
@property (weak, nonatomic) IBOutlet UIImageView *dropDownBgImage;
@property (weak, nonatomic) IBOutlet UIButton *btnDropdown;

@end
