//
//  EmailListingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "EventListViewController.h"
#import "EventListCustomCell.h"

#define USE_MG_DELEGATE 1
@interface EventListViewController (){
    
    NSMutableArray *swipeCellButtons, *controlls;
    NSDictionary *eventDetails;
    NSMutableArray *arrayEventsList;
    NSArray *eventtypeToShowArray;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
}
@end

@implementation EventListViewController
@synthesize tableviewEventList;

- (void)viewDidLoad {
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue906"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // Do any additional setup after loading the view from its nib.
    [self callServiceToGetEventlist];
    self.isNeedToRefresh=FALSE;
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
        [self callServiceToGetEventlist];
    }
    
}
#pragma - mark WebService Call
-(void)callServiceToGetEventlist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetEventAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
    
}

-(void)callServiceDeleteEventlist {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"EventEntryID" : [eventDetails valueForKey:@"EventEntryID"]
                                 
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteEventDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Delete;
    
}

-(void)callServiceEditEvent {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"EventEntryID" : [NSString stringWithFormat:@"%@",[eventDetails valueForKey:@"EventEntryID"]]
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_EditEventDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Edit;
    
}


#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if (_serviceCalled == Service_Called_Delete)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            
            [arrayEventsList removeObjectAtIndex:currentIndexPathToDelete.row];
            
            [tableviewEventList beginUpdates];
            [tableviewEventList deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [tableviewEventList endUpdates];
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    else if(_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstEventGrdList"] isKindOfClass:[NSNull class]])
            {
                arrayEventsList = [[responseDictionery objectForKey:@"lstEventGrdList"] mutableCopy];
                //If Array Has the data then only Reload Data.
                if([arrayEventsList count] > 0) {
                    [self.tableviewEventList reloadData];
                }
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_Edit)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            
            AddEventViewController *event = [[AddEventViewController alloc]initWithNibName:@"AddEventViewController" bundle:nil];
            
            //Show, User Wants to add details or edit details.
            event.serviceCalled = Service_Called_Edit;
            currentIndexPathToDelete = nil;
            event.eventDetails = responseDictionery;
            event.playerDetails = _playerDetails;
            event.owner=self;
            [self.navigationController pushViewController:event animated:YES];
            
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"Ok" InView:self];
        }
        
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/**************************** TABLEVIEW Functions ****************************************************/
/********************************************************************************************************/
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrayEventsList.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrayEventsList count]) {
        if(IS_IPAD)
            return 124;
        else
            return 94;
    }
    else{
        if(IS_IPAD)
            return 84;
        else
            return 60;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrayEventsList count])
    {
        static NSString *itemCell = @"EventListCustomCell";
        EventListCustomCell *cell = (EventListCustomCell *)[self.tableviewEventList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EventListCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        cell.delegate = self;
        if(IS_IPAD){
            [cell.lblEventNameText setFont:app_font_bold_24];
            [cell.lblPositionText setFont:app_font24];
            [cell.lblFieldText setFont:app_font18];
            [cell.btnFormSubmitted.titleLabel setFont:app_font_bold_18];
        }
        else if(IS_IPHONE){
            [cell.lblEventNameText setFont:app_font_bold_20];
            [cell.lblPositionText setFont:app_font20];
            [cell.lblFieldText setFont:app_font15];
            [cell.btnFormSubmitted.titleLabel setFont:app_font_bold_15];
        }
        
        NSDictionary *dict = [arrayEventsList objectAtIndex:indexPath.row];
        
        bool IsFormSubmitted =  [[dict valueForKey:@"EntryFormSubmitted"] boolValue];
        
        cell.lblEventNameText.text = [NSString stringWithFormat:@"%@",[dict valueForKey:@"Event"]];
        
        cell.lblPositionText.text = [NSString stringWithFormat:@"Position : %@",[[dict valueForKey:@"PositionInField"]isKindOfClass:[NSNull class]]?@" ":[dict valueForKey:@"PositionInField"]];
        
        cell.lblFieldText.text = [NSString stringWithFormat:@"Field : %@",[[dict valueForKey:@"SizeOfField"] isKindOfClass:[NSNull class]]?@" ":[dict valueForKey:@"SizeOfField"]];
        
        if (IsFormSubmitted == true) {
            [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
            [cell.btnFormSubmitted setTitle:@"FORM SUBMITTED" forState:UIControlStateNormal];
            
        } else {
            [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
            [cell.btnFormSubmitted setTitle:@"FORM PENDING" forState:UIControlStateNormal];
        }
        
        //        NSString *formSubmittedString = [cell.btnFormSubmitted.currentTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        //
        //        CGSize stringsize = [formSubmittedString sizeWithFont:cell.btnFormSubmitted.titleLabel.font];
        //        cell.btnFormSubmitted.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        //        cell.btnFormSubmitted.contentEdgeInsets = UIEdgeInsetsMake(0,17,0,0);
        
        NSString *formSubmittedString = [cell.btnFormSubmitted.currentTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat buttonWidth = [COMMON_SETTINGS getWidthForText:formSubmittedString withFont:cell.btnFormSubmitted.titleLabel.font andHeight:cell.btnFormSubmitted.frame.size.height minWidth:30];
        cell.btnFormSubmittedWidth.constant = buttonWidth + 30;
        [cell.btnFormSubmitted layoutIfNeeded];
        
        for (CALayer *layer in cell.viewFormBack.layer.sublayers) {
            [layer removeFromSuperlayer];
        }
        CAShapeLayer *labelView = [self GetLableForFormSubmitedBackgroundInFrame:cell.btnFormSubmitted.frame];
        [cell.viewFormBack.layer addSublayer:labelView];
        
        if ([cell.btnFormSubmitted.currentTitle isEqualToString:@"FORM PENDING"]) {
            labelView.fillColor = [UIColor colorWithRed:0.7804 green:0.7804 blue:0.7804 alpha:1.0].CGColor;
            [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
        }
        else {
            labelView.fillColor = [UIColor colorWithRed:0.8784 green:0.6510 blue:0.0627 alpha:1.0].CGColor;
            [cell.btnFormSubmitted setTitleColor:color_black forState:UIControlStateNormal];
        }
        
        return cell;
    }
    else
    {
        static NSString *itemCell = @"FooterCustomCell";
        FooterCustomCell *cell = (FooterCustomCell *)[self.tableviewEventList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FooterCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.titleLabel.text = @"Add Event";
        }
        return cell;
    }
    
}

-(CAShapeLayer*)GetLableForFormSubmitedBackgroundInFrame:(CGRect)frame
{
    NSInteger arrowSize;
    
    if(IS_IPAD){
        arrowSize = 17;
    }else{
        arrowSize = 13;
    }
    CGPoint point1 = CGPointMake(0,frame.size.height/2);
    CGPoint point2 = CGPointMake(frame.size.height/2, 0);
    CGPoint point3 = CGPointMake(frame.size.width, 0);
    CGPoint point4 = CGPointMake(frame.size.width, frame.size.height);
    CGPoint point5 = CGPointMake(frame.size.height/2, frame.size.height);
    
    CAShapeLayer *line = [CAShapeLayer layer];
    UIBezierPath *linePath=[UIBezierPath bezierPath];
    [linePath moveToPoint:point1];
    [linePath addLineToPoint:point2];
    [linePath addLineToPoint:point3];
    [linePath addLineToPoint:point4];
    [linePath addLineToPoint:point5];
    [linePath addLineToPoint:point1];
    
    line.lineWidth = 1.5f;
    line.path=linePath.CGPath;
    line.fillColor = [UIColor colorWithRed:0.1019 green:0.3882 blue:0.6666 alpha:0.50].CGColor;
    return line;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [arrayEventsList count]){
        
        AddEventViewController *event = [[AddEventViewController alloc]initWithNibName:@"AddEventViewController" bundle:nil];
        event.isLogedInUsersDetail = _isLogedInUsersDetail;
        event.playerDetails = _playerDetails;
        //Show, User Wants to add details or edit details.
        event.serviceCalled = Service_Called_Add;
        event.owner=self;
        [self.navigationController pushViewController:event animated:YES];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
/***********************************************************************/

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray *result = [NSMutableArray array];
    NSString *titles[2] = {@"DELETE", @"EDIT"};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *edit = [UIImage imageNamed:@"edit"];
    UIImage *icons[2] = {delete,edit};
    
    UIColor *colors[2] = {[UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1], [UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1]};
    
    for (int i = 0; i < 2; ++i)
    {
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    
    CGPoint buttonPosition = [cell.swipeContentView convertPoint:CGPointZero
                                                          toView:self.tableviewEventList];
    NSIndexPath *indexPath = [self.tableviewEventList indexPathForRowAtPoint:buttonPosition];
    eventDetails = [arrayEventsList objectAtIndex:indexPath.row];
    
    NSDictionary *dict = [arrayEventsList objectAtIndex:indexPath.row];
    bool IsPrimary =  [[dict valueForKey:@"IsPrimary"] boolValue];
    
    if (IsPrimary == true) {
        
        return nil;
    }
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:data.rightButtonsCount];
    }
}
#endif

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    
    NSIndexPath *indexPath = [self.tableviewEventList indexPathForCell:cell];
    //NSLog(@"%@",indexPath);
    eventDetails = [arrayEventsList objectAtIndex:indexPath.row];
    
    
    if (index == 0) {
        // Show popup.
        
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete Your Email Details?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         //Is User Wants to delete Email Details Then Continiue.
                                                                         //NSLog(@"Delete");
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDeleteEventlist];
                                                                         
                                                                     }];
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // code to be written for version lower than ios 8.0...
        }
        //------------------------------------------------------------------------
        
    } else {
        
        [self callServiceEditEvent];
    }
    
    return YES;
}

@end
