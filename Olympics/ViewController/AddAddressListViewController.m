//
//  addAddressListViewController.m
//  Olympics
//
//  Created by webwerks2 on 7/14/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AddAddressListViewController.h"
#import "AddressListingViewController.h"

static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 236;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;
@interface AddAddressListViewController ()
{
    CGFloat animatedDistance;
    NSString *previousText,*currentText;
}
@end

@implementation AddAddressListViewController
@synthesize incrementedHeight, controlls ,typesArray,statesArray,countriesArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    _isFirstTime = true;
    
    // Do any additional setup after loading the view from its nib.
    _titleLabel.font = app_font_bold_15;
    _navigatiobBarLogi.text = [NSString stringWithFormat:@"\ue910"];
    self.isChangesMade = FALSE;
    
    //Create Require Fields.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    if(_isFirstTime)
    {
        [self StartAddingControllers];
        _isFirstTime = false;
    }
}


- (IBAction)DidSelectBackButton:(id)sender {
    
    if ([self.owner class]==[AddressListingViewController class]) {
        [(AddressListingViewController *)self.owner setIsNeedToRefresh:FALSE];
    }
    
    if(self.isChangesMade == TRUE)
    {
        [self callMethodToShowChangesAlert];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (IBAction)DidSelectSaveButton:(id)sender
{
    [self CallMethodToSaveData];
}

-(void)CallMethodToSaveData
{
    NSMutableDictionary *requestParameters = [[NSMutableDictionary alloc] init];
    for (int i = 0 ; i < [self.controlls count]; i++)
    {
        if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMDropDown class]])
        {
            OLMDropDown *DropdownObj  = (OLMDropDown*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(DropdownObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
            else
            {
                NSDictionary *answersDictDefault = [DropdownObj GetHardcodedAnswer];
                [requestParameters addEntriesFromDictionary:answersDictDefault];
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMTextField class]])
        {
            OLMTextField *TextFieldObj  = (OLMTextField*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(TextFieldObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
        else  if([[self.controlls objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[self.controlls objectAtIndex:i];
            NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                [requestParameters addEntriesFromDictionary:answersDict];
            }
            else if(CheckBoxObj.IsRequired == 1)
            {
                [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                [requestParameters removeAllObjects];
                return;
            }
        }
    }
    
    
    [self CallServiceToSaveDataWithDictionary:requestParameters];
}

#pragma - mark WebService Call
-(void)CallServiceToSaveDataWithDictionary:(NSMutableDictionary*)userinformation
{
    [COMMON_SETTINGS showHUD];
    
    if(_serviceCalled == Service_Called_Edit){
        
        NSString *valueID = [_addressDetails valueForKey:@"AddressId"];
        [userinformation setValue:valueID forKey:@"AddressId"];
    }
    
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_InsertUpdateAddressDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:userinformation] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    if([[responseDictionery valueForKey:@"Status"] intValue] == 1)
    {
        
        if ([self.owner class]==[AddressListingViewController class]) {
            [(AddressListingViewController *)self.owner setIsNeedToRefresh:TRUE];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
    }
    
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

#pragma mark - Method to add questions on screen
-(void)StartAddingControllers {
    
    //Field_DropdownList.
    incrementedHeight = 20;
    controlls = [[NSMutableArray alloc]init];
    CGRect frame = self.baseScrollView.frame;
    
    if(![typesArray isKindOfClass:[NSNull class]] && typesArray && [typesArray count]>0)
    {
        NSInteger initialIndex = 0;
        if(_serviceCalled == Service_Called_Edit)
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"Key == %@",_addressDetails[@"AddressTypeId"]];
            NSArray *filteredValues = [typesArray filteredArrayUsingPredicate:filterPredicate];
            if(filteredValues.count){
                initialIndex = [[filteredValues lastObject][@"Key"]integerValue];
            }
        }
        
        OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Type" fieldIdName:@"AddressTypeId" questionId:101 Options:typesArray IsRequired:YES defaultValue:initialIndex delegate:self];
        controlObj.questionTypeId = Field_DropdownList;
        controlObj.tag = 0;
        [controlls addObject:controlObj];
        [self.baseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    // Field_TextField.
    OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Address" fieldIdName:@"Address" questionId:102 IsRequired:YES defaultValue: (_serviceCalled == Service_Called_Edit) ?(_addressDetails[@"Address"]?_addressDetails[@"Address"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    controlObj.questionTypeId = Field_TextField;
    controlObj.tag = 1;
    [controlls addObject:controlObj];
    [self.baseScrollView addSubview:controlObj];
    incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    
    // Field_TextField.
    OLMTextField *cityObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"City" fieldIdName:@"City" questionId:103 IsRequired:YES defaultValue:(_serviceCalled == Service_Called_Edit) ?(_addressDetails[@"City"]?_addressDetails[@"City"]:@""):@"" keyboardType:UIKeyboardTypeDefault  delegate:self];
    
    cityObj.questionTypeId = Field_TextField;
    cityObj.tag = 2;
    [controlls addObject:cityObj];
    [self.baseScrollView addSubview:cityObj];
    incrementedHeight = incrementedHeight + cityObj.frame.size.height + 5;
    
    //Field_DropdownList.
    if(![statesArray isKindOfClass:[NSNull class]] && statesArray && [statesArray count]>0)
    {
        
        NSInteger initialIndex = 0;
        if(_serviceCalled == Service_Called_Edit)
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"Key == %@",_addressDetails[@"StateId"]];
            NSArray *filteredValues = [statesArray filteredArrayUsingPredicate:filterPredicate];
            if(filteredValues.count){
                initialIndex = [[filteredValues lastObject][@"Key"]integerValue];
            }
        }
        
        OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"State" fieldIdName:@"StateId" questionId:104 Options:statesArray IsRequired:YES defaultValue:initialIndex delegate:self];
        controlObj.questionTypeId = Field_DropdownList;
        controlObj.tag = 3;
        [controlls addObject:controlObj];
        [self.baseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    //Field_DropdownList.
    if(![countriesArray isKindOfClass:[NSNull class]] && countriesArray && [countriesArray count]>0)
    {
        
        NSInteger initialIndex = 0;
        if(_serviceCalled == Service_Called_Edit)
        {
            NSPredicate *filterPredicate = [NSPredicate predicateWithFormat:@"Key == %@",_addressDetails[@"CountryId"]];
            NSArray *filteredValues = [countriesArray filteredArrayUsingPredicate:filterPredicate];
            if(filteredValues.count){
                initialIndex = [[filteredValues lastObject][@"Key"]integerValue];
            }
        }
        
        OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"Country" fieldIdName:@"CountryId" questionId:105 Options:countriesArray IsRequired:YES defaultValue:initialIndex delegate:self];
        controlObj.questionTypeId = Field_DropdownList;
        controlObj.tag = 4;
        [controlls addObject:controlObj];
        [self.baseScrollView addSubview:controlObj];
        incrementedHeight = incrementedHeight + controlObj.frame.size.height + 5;
    }
    else
    {
        //NSLog(@"Option Array Not Found");
    }
    
    // Field_TextField.
    OLMTextField *postcodeObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) Question:@"PostCode" fieldIdName:@"PostCode" questionId:106 IsRequired:YES defaultValue:(_serviceCalled == Service_Called_Edit) ?(_addressDetails[@"PostCode"]?_addressDetails[@"PostCode"]:@""):@"" keyboardType:UIKeyboardTypeNumberPad  delegate:self];
    
    postcodeObj.questionTypeId = Field_NumericTextField;
    postcodeObj.tag = 5;
    [controlls addObject:postcodeObj];
    [self.baseScrollView addSubview:postcodeObj];
    incrementedHeight = incrementedHeight + postcodeObj.frame.size.height + 5;
    
    // Chech_Box_Field.
    OLMSingleCheckBox *checkBoxOneObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(10, incrementedHeight, frame.size.width-20, 1) questionId:107 fieldIdName:@"IsPrimary" OptionTitle:@"Primary" isRequired:YES defaultSelected:[_addressDetails[@"IsPrimary"] boolValue] delegate:self];
    checkBoxOneObj.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj.tag = 6;
    [controlls addObject:checkBoxOneObj];
    [self.baseScrollView addSubview:checkBoxOneObj];
    incrementedHeight = incrementedHeight + checkBoxOneObj.frame.size.height + 5;
    
    self.isChangesMade = FALSE;
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMSingleCheckBox:(OLMSingleCheckBox *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    self.isChangesMade = TRUE;
}
#pragma mark - OLMTextView Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    previousText = textField.text;
    [self scrollUp:textField];
    self.isChangesMade = TRUE;
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    currentText = textField.text;
    [self scrollDown];
//    if(![previousText isEqualToString:currentText])
//    {
//        self.isChangesMade = TRUE;
//    }
}

#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.baseScrollView.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.baseScrollView.window convertRect:self.baseScrollView.bounds fromView:self.baseScrollView];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
    
}

-(void)scrollDown {
    
    CGRect viewFrame = self.baseScrollView.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.baseScrollView setFrame:viewFrame];
    [UIView commitAnimations];
}

#pragma mark - Method Implementation to show changes alert
-(void)callMethodToShowChangesAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Do you want to save the changes ?"preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"CANCEL" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action)
                                   {
                                       [self.navigationController popViewControllerAnimated:YES];
                                   }];
    [alert addAction:cancelAction];
    
    UIAlertAction *saveAction = [UIAlertAction actionWithTitle:@"SAVE" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                 {
                                     [self CallMethodToSaveData];
                                 }];
    [alert addAction:saveAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
