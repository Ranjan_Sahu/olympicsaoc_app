//
//  EmailListingViewController.m
//  Olympics
//
//  Created by webwerks on 11/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "NextOfKinListViewController.h"
#import "EventListCustomCell.h"

#define USE_MG_DELEGATE 1
@interface NextOfKinListViewController (){
    
    NSMutableArray *swipeCellButtons, *controlls;
    NSDictionary *NextOfKinDetails;
    NSMutableArray *arrayNextOfKinList;
    NSArray *eventtypeToShowArray;
    UITableViewCellAccessoryType accessory;
    NSIndexPath *currentIndexPathToDelete;
}
@end

@implementation NextOfKinListViewController
@synthesize tableviewNextOfKinList;

- (void)viewDidLoad {
    
    [super viewDidLoad];
    swipeCellButtons = [TestData data];
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue911"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self callServiceToGetNextOfKinList];
    
}

- (IBAction)DidSelectBackButton:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    if (self.isNeedToRefresh)
    {
        self.isNeedToRefresh=false;
        [self callServiceToGetNextOfKinList];
    }
    
}
#pragma - mark WebService Call

-(void)callServiceToGetNextOfKinList {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = [[NSDictionary alloc] init];
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_GetNextOfKinAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_GetDetail;
    
    
}

-(void)callServiceDeleteNextOfKinList {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"NextOfKinId" : [NSString stringWithFormat:@"%@",[NextOfKinDetails valueForKey:@"NextOfKinId"]]
                                 
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_DeleteNextOfKinAccrDetails withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Delete;
    
}

-(void)callServiceEditNextOfKin {
    
    [COMMON_SETTINGS showHUD];
    NSDictionary *parameters = @{
                                 @"NextOfKinId" : [NSString stringWithFormat:@"%@",[NextOfKinDetails valueForKey:@"NextOfKinId"]]
                                 };
    Service *callService = [[Service alloc] init];
    callService.delegate = self;
    [callService requestingURLString:weburl Service:Service_EditNextOfKinList withParameters:[NSMutableDictionary dictionaryWithDictionary:parameters] ContactID:[_playerDetails valueForKey:@"ContactID"]];
    _serviceCalled = Service_Called_Edit;
    
}

#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    NSError *error;
    
    if (_serviceCalled == Service_Called_Delete)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            [arrayNextOfKinList removeObjectAtIndex:currentIndexPathToDelete.row];
            
            [tableviewNextOfKinList beginUpdates];
            [tableviewNextOfKinList deleteRowsAtIndexPaths:[NSArray arrayWithObject:currentIndexPathToDelete] withRowAnimation:UITableViewRowAnimationNone];
            [tableviewNextOfKinList endUpdates];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else if(_serviceCalled == Service_Called_GetDetail)
    {
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            if(![[responseDictionery objectForKey:@"lstKinList"] isKindOfClass:[NSNull class]])
            {
                arrayNextOfKinList = [[responseDictionery objectForKey:@"lstKinList"] mutableCopy];
                //If Array Has the data then only Reload Data.
                if([arrayNextOfKinList count] > 0) {
                    [self.tableviewNextOfKinList reloadData];
                }
            }
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
    else if (_serviceCalled == Service_Called_Edit)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            
            currentIndexPathToDelete = nil;
            
            AddNextOfKinViewController *nextOfKin = [[AddNextOfKinViewController alloc]initWithNibName:@"AddNextOfKinViewController" bundle:nil];
            nextOfKin.isLogedInUsersDetail = _isLogedInUsersDetail;
            nextOfKin.playerDetails = _playerDetails;
            //Show, User Wants to add details or edit details.
            nextOfKin.serviceCalled = Service_Called_Edit;
            nextOfKin.nextOfKinDetails = responseDictionery;
            nextOfKin.owner=self;
            [self.navigationController pushViewController:nextOfKin animated:YES];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
        
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

/*****************************************TABLEVIEW Functions ****************************************************/
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return arrayNextOfKinList.count + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row < [arrayNextOfKinList count]) {
        
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewNextOfKinList dequeueReusableCellWithIdentifier:itemCell];
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        CGFloat cellHeight = 30.0f;
        NSDictionary *dictionary = [arrayNextOfKinList objectAtIndex:indexPath.row];
        NSString *descriptionString = [[dictionary valueForKey:@"Relationship"] isKindOfClass:[NSNull class]]?@"":[[dictionary valueForKey:@"Relationship"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"KinName"] isKindOfClass:[NSNull class]]?@"":[[dictionary valueForKey:@"KinName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cellHeight = titleHeight + descriptionHeight + cellHeight;
        return cellHeight;
        
    }
    else{
        if(IS_IPAD){
            return 84;
        }
        else{
            return 60;
        }
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row < [arrayNextOfKinList count])
    {
        static NSString *itemCell = @"addressListTableViewCell";
        addressListTableViewCell *cell = (addressListTableViewCell *)[self.tableviewNextOfKinList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"addressListTableViewCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
        }
        
        NSDictionary *dictionary = [arrayNextOfKinList objectAtIndex:indexPath.row];
        NSString *descriptionString = [[dictionary valueForKey:@"Relationship"] isKindOfClass:[NSNull class]]?@"":[[dictionary valueForKey:@"Relationship"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        NSString *titleString = [[dictionary valueForKey:@"KinName"] isKindOfClass:[NSNull class]]?@"":[[dictionary valueForKey:@"KinName"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        CGFloat titleHeight = [COMMON_SETTINGS getHeightForText:titleString withFont:cell.lblAddressName.font andWidth:tableView.frame.size.width-30 minHeight:cell.titleHeightConstraint.constant];
        
        CGFloat descriptionHeight = [COMMON_SETTINGS getHeightForText:descriptionString withFont:cell.lblDisplayAddress.font andWidth:tableView.frame.size.width-30 minHeight:cell.descriptionHeightConstraint.constant];
        
        cell.titleHeightConstraint.constant = titleHeight;
        cell.descriptionHeightConstraint.constant = descriptionHeight;
        
        cell.lblAddressName.text = titleString;
        cell.lblDisplayAddress.text = descriptionString;
        
        bool IsPrimary =  [[dictionary valueForKey:@"IsPrimary"] boolValue];
        cell.lblLock.text = @"\ue903";
        
        cell.lblAddressName.text = titleString;
        cell.lblDisplayAddress.text = descriptionString;
        
        if (IsPrimary == true) {
            cell.lblLock.alpha = 0.7;
            cell.lblLock.hidden  = FALSE;
        } else {
            cell.lblLock.alpha = 0.5;
            cell.lblLock.hidden  = TRUE;
        }
        
        cell.delegate = self;
        return cell;
    }
    else
    {
        static NSString *itemCell = @"FooterCustomCell";
        FooterCustomCell *cell = (FooterCustomCell *)[self.tableviewNextOfKinList dequeueReusableCellWithIdentifier:itemCell];
        
        if (cell == nil)
        {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FooterCustomCell" owner:nil options:nil];
            cell =  [nib objectAtIndex:0];
            cell.backgroundColor = [UIColor clearColor];
            cell.titleLabel.text = @"Add Next Of Kin";
        }
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row == [arrayNextOfKinList count]){
        
        AddNextOfKinViewController *nextOfKin = [[AddNextOfKinViewController alloc]initWithNibName:@"AddNextOfKinViewController" bundle:nil];
        nextOfKin.isLogedInUsersDetail = _isLogedInUsersDetail;
        nextOfKin.playerDetails = _playerDetails;
        //Show, User Wants to add details or edit details.
        nextOfKin.serviceCalled = Service_Called_Add;
        nextOfKin.owner=self;
        [self.navigationController pushViewController:nextOfKin animated:YES];
        
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    }
}
/*****************************************************************************/

-(NSArray *) createRightButtons: (int) number
{
    NSMutableArray *result = [NSMutableArray array];
    NSString *titles[2] = {@"DELETE", @"EDIT"};
    
    UIImage *delete = [UIImage imageNamed:@"delete"];
    UIImage *edit = [UIImage imageNamed:@"edit"];
    UIImage *icons[2] = {delete,edit};
    
    UIColor *colors[2] = {[UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1], [UIColor colorWithRed:0.0000 green:0.3961 blue:0.6824 alpha:1]};
    
    for (int i = 0; i < 2; ++i)
    {
        MGSwipeButton  *button = [MGSwipeButton buttonWithTitle:titles[i] icon:icons[i]  backgroundColor:colors[i] padding:60 callback:^BOOL(MGSwipeTableCell  *sender)
                                  {
                                      return YES;
                                  }];
        
        CGRect frame = button.frame;
        frame.size.width = [UIScreen mainScreen].bounds.size.width/4;
        button.frame = frame;
        [result addObject:button];
    }
    return result;
}

#if USE_MG_DELEGATE
-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings;
{
    
    CGPoint buttonPosition = [cell.swipeContentView convertPoint:CGPointZero
                                                          toView:self.tableviewNextOfKinList];
    NSIndexPath *indexPath = [self.tableviewNextOfKinList indexPathForRowAtPoint:buttonPosition];
    NextOfKinDetails = [arrayNextOfKinList objectAtIndex:indexPath.row];
    
    NSDictionary *dict = [arrayNextOfKinList objectAtIndex:indexPath.row];
    bool IsPrimary =  [[dict valueForKey:@"IsPrimary"] boolValue];
    
    if (IsPrimary == true) {
        
        return nil;
    }
    TestData *data = [swipeCellButtons objectAtIndex:0];
    swipeSettings.transition = data.transition;
    
    if (direction == MGSwipeDirectionLeftToRight)
    {
        expansionSettings.buttonIndex = data.leftExpandableIndex;
        expansionSettings.fillOnTrigger = NO;
        return 0;
    }
    else
    {
        expansionSettings.buttonIndex = data.rightExpandableIndex;
        expansionSettings.fillOnTrigger = YES;
        return [self createRightButtons:data.rightButtonsCount];
    }
}
#endif

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
//    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
//          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    
    NSIndexPath *indexPath = [self.tableviewNextOfKinList indexPathForCell:cell];
    //NSLog(@"%@",indexPath);
    NextOfKinDetails = [arrayNextOfKinList objectAtIndex:indexPath.row];
    
    
    if (index == 0) {
        // Show popup.
        
        if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                           message:@"Are you sure you want the Delete Your Email Details?"
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"YES"
                                                                       style:(UIAlertActionStyleDefault)
                                               
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                         //Is User Wants to delete Email Details Then Continiue.
                                                                         //NSLog(@"Delete");
                                                                         currentIndexPathToDelete = indexPath;
                                                                         [self callServiceDeleteNextOfKinList];
                                                                         
                                                                     }];
            UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"NO"
                                                                      style:(UIAlertActionStyleCancel)
                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                        //
                                                                    }];
            [alert addAction:alert_yes_action];
            [alert addAction:alert_no_action];
            [self presentViewController:alert animated:YES completion:nil];
        } else {
            // code to be written for version lower than ios 8.0...
        }
        //------------------------------------------------------------------------
        
    } else {
        
        [self callServiceEditNextOfKin];
        
    }
    
    return YES;
}

@end
