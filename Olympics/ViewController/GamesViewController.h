//
//  GamesViewController.h
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Question_Object.h"
#import "OLMLable.h"
#import "OLMCheckBox.h"
#import "OLMDropDown.h"
#import "OLMTextField.h"
#import "OLMRadioButton.h"
#import "OLMDatePicker.h"
#import "Service.h"
#import "OLMRadioButtonTextKeyType.h"

@interface GamesViewController : UIViewController<Service_delegate>

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (nonatomic, retain) NSMutableArray *fieldsToShowArray;
@property(nonatomic) NSInteger incrementedHeight;
@property(nonatomic, strong) NSMutableArray *controlls;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *vanigationBarView;
@property (weak, nonatomic) IBOutlet UILabel *navigatiobBarLogi;
@property(nonatomic, assign) Service_Called serviceCalled;
@property (nonatomic, strong) NSDictionary *playerDetails;
@property (assign,nonatomic) BOOL isLogedInUsersDetail;
@property (assign,nonatomic) BOOL isChangesMade;

- (IBAction)DidSelectBackButton:(id)sender;
- (IBAction)DidSelectSaveButton:(id)sender;


@end
