//
//  NoInternetAccessViewController.m
//  Olympics
//
//  Created by webwerks on 12/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "NoInternetAccessViewController.h"

@interface NoInternetAccessViewController ()

@end

@implementation NoInternetAccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self SetFontForSubviews];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark Set Up subviews

-(void)SetFontForSubviews
{
    [_TitleLabel setFont:app_font_bold_18];
    [_messageLabel setFont:app_font15];
    [_retryButton.titleLabel setFont:app_font_bold_18];
    
}

- (IBAction)DidSelectRetry:(id)sender
{
    if([COMMON_SETTINGS isInternetReachable])
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}
@end
