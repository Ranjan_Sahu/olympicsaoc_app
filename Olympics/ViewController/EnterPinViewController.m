//
//  EnterPinViewController.m
//  Olympics
//
//  Created by webwerks on 06/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "EnterPinViewController.h"
#import "HomeScreenViewController.h"
#import "MyGamesViewController.h"

@interface EnterPinViewController ()
{
    int pinEntryFlag;
    NSString *enteredPin, *confirmedPin, *acessPin;
}
@end

@implementation EnterPinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self SetFontForSubviews];
    UITapGestureRecognizer *tapOnBkView =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnBackGroundView:)];
    [self.view addGestureRecognizer:tapOnBkView];
    UITapGestureRecognizer *tapOnView =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnUserPinInView:)];
    [_viewUserPin addGestureRecognizer:tapOnView];
    [_txtUserPin addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewDidAppear:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];

    [self.navigationController.navigationBar setHidden:TRUE];
    
    _txtPin1.text = @"";
    _txtPin2.text = @"";
    _txtPin3.text = @"";
    _txtPin4.text = @"";
    _txtUserPin.text = @"";
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:UD_UserSecretPin] length] == 0)
    {
        // If PIN is not generated
        _lblEnterPin.text = @"Create PIN";
        _lblPinRecieved.text = @"Enter 4 digit confidential PIN number";
        [_btnLogin setTitle:@"CONTINUE" forState:UIControlStateNormal];
        pinEntryFlag = 0;
        _btnForgotPIN.hidden = TRUE;
    }
    else
    {
        // If PIN is already generated
        _lblEnterPin.text = @"Enter PIN Number";
        _lblPinRecieved.text = @"Enter 4 digit confidential PIN number";
        [_btnLogin setTitle:@"LOGIN" forState:UIControlStateNormal];
        pinEntryFlag = 2;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [_txtUserPin becomeFirstResponder];
        
    }];
}

#pragma - mark Set Up subviews

-(void)SetFontForSubviews
{
    //    [_txtPin1 setFont:app_font_bold_24];
    //    [_txtPin2 setFont:app_font_bold_24];
    //    [_txtPin3 setFont:app_font_bold_24];
    //    [_txtPin4 setFont:app_font_bold_24];
    //    [_lblEnterPin setFont:app_font_bold_21];
    //    [_lblPinRecieved setFont:app_font12];
    //    [_btnLogin.titleLabel setFont:app_font_bold_18];
    
}

#pragma - mark UITextField Deleget

- (void)textFieldChanged:(UITextField *)textField
{
    if (textField.text.length == 0) {
        self.txtPin1.text=@"";
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    switch (newString.length)
    {
        case 1:
            self.txtPin1.text = [newString substringWithRange:NSMakeRange(0, 1)];
            self.txtPin2.text=@"";
            self.txtPin3.text=@"";
            self.txtPin4.text=@"";
            break;
        case 2:
            self.txtPin2.text = [newString substringWithRange:NSMakeRange(1, 1)];
            self.txtPin3.text=@"";
            self.txtPin4.text=@"";
            break;
        case 3:
            self.txtPin3.text = [newString substringWithRange:NSMakeRange(2, 1)];
            self.txtPin4.text=@"";
            break;
        case 4:
            self.txtPin4.text=nil;
            self.txtPin4.text = [newString substringWithRange:NSMakeRange(3, 1)];
            
            break;
        default:
            break;
    }
    
    if(newString.length == 4)
    {
        if(pinEntryFlag == 2)
        {
            acessPin = newString;

            if([[[NSUserDefaults standardUserDefaults] valueForKey:UD_UserSecretPin] isEqual:newString])
            {
                MyGamesViewController *myGamesView = [[MyGamesViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"MyGamesViewController"] bundle:nil];
                
                [self.navigationController pushViewController:myGamesView animated:YES];
            }
            else
            {
                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Incorrect User PIN, Please Re-enter" CancelButtonTitle:@"OK" InView:self];
                _txtPin1.text = @"";
                _txtPin2.text = @"";
                _txtPin3.text = @"";
                _txtPin4.text = @"";
                _txtUserPin.text = @"";
            }
        }
        else if(pinEntryFlag == 0)
        {
            enteredPin = newString;
            _lblEnterPin.text = @"Confirm Your PIN";
            _lblPinRecieved.text = @"Confirm the PIN number you have entered";
            _txtPin1.text = @"";
            _txtPin2.text = @"";
            _txtPin3.text = @"";
            _txtPin4.text = @"";
            _txtUserPin.text = @"";
            [_btnLogin setTitle:@"CONFIRM" forState:UIControlStateNormal];
            pinEntryFlag = 1;
            return FALSE;
        }
        else if(pinEntryFlag == 1)
        {
            confirmedPin = newString;
            
            if([enteredPin isEqual:confirmedPin])
            {
                [[NSUserDefaults standardUserDefaults] setValue:confirmedPin forKey:UD_UserSecretPin];
                
                MyGamesViewController *myGamesView = [[MyGamesViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"MyGamesViewController"] bundle:nil];
                [self.navigationController pushViewController:myGamesView animated:YES];
            }
            else
            {
                //NSLog(@"Incorrect PIN: Please Re-enter");
                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"PIN Mismatched, Please Re-enter" CancelButtonTitle:@"OK" InView:self];
                _txtPin1.text = @"";
                _txtPin2.text = @"";
                _txtPin3.text = @"";
                _txtPin4.text = @"";
                _txtUserPin.text = @"";
            }
        }
    }
    return (newString.length<=4);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        [textField resignFirstResponder];
        
    }];
    
    return TRUE;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return TRUE;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return TRUE;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
}
#pragma mark
#pragma mark Tap Gesture Delegate
- (void)tapOnUserPinInView:(UITapGestureRecognizer *)recognizer {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [_txtUserPin becomeFirstResponder];
        
    }];
    
}
- (void)tapOnBackGroundView:(UITapGestureRecognizer *)recognizer {
    
    [UIView animateWithDuration:0.5 animations:^{
        
        [_txtUserPin resignFirstResponder];
        
    }];
    
}

#pragma - mark Button Action

- (IBAction)onClickLogin:(UIButton *)sender
{
    if([COMMON_SETTINGS validateOnlyNumeric:_txtUserPin.text])
    {
        if(_txtUserPin.text.length == 0)
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Please Enter pin" CancelButtonTitle:@"Ok" InView:self];
        }
        else if (_txtUserPin.text.length<4) {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"PIN must be of 4 digits" CancelButtonTitle:@"OK" InView:self];
            return;
        }
//        else if(pinEntryFlag == 2)
//        {
//            acessPin = _txtUserPin.text;
//            [_txtUserPin resignFirstResponder];
//            
//            if([[[NSUserDefaults standardUserDefaults] valueForKey:UD_UserSecretPin] isEqual:acessPin])
//            {
//                //NSLog(@"Success:");
//                /*
//                HomeScreenViewController *homeView = [[HomeScreenViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"HomeScreenViewController"] bundle:nil];
//                APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
//                APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
//                [APP_DELEGATE.window makeKeyAndVisible];
//                 */
//                
//                MyGamesViewController *myGamesView = [[MyGamesViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"MyGamesViewController"] bundle:nil];
//                
//                [self.navigationController pushViewController:myGamesView animated:YES];
//            }
//            else
//            {
//                //NSLog(@"Incorrect Pin: Please Re-enter");
//                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Incorrect User PIN, Please Re-enter" CancelButtonTitle:@"OK" InView:self];
//                _txtPin1.text = @"";
//                _txtPin2.text = @"";
//                _txtPin3.text = @"";
//                _txtPin4.text = @"";
//                _txtUserPin.text = @"";
//            }
//        }
//        else if(pinEntryFlag == 0)
//        {
//            enteredPin = _txtUserPin.text;
//            
//            _lblEnterPin.text = @"Confirm Your PIN";
//            _lblPinRecieved.text = @"Confirm the PIN number you have entered";
//            _txtPin1.text = @"";
//            _txtPin2.text = @"";
//            _txtPin3.text = @"";
//            _txtPin4.text = @"";
//            _txtUserPin.text = @"";
//            [_btnLogin setTitle:@"CONFIRM" forState:UIControlStateNormal];
//            pinEntryFlag = 1;
//        }
//        else if(pinEntryFlag == 1)
//        {
//            confirmedPin = _txtUserPin.text;
//            
//            if([enteredPin isEqual:confirmedPin])
//            {
//                [[NSUserDefaults standardUserDefaults] setValue:confirmedPin forKey:UD_UserSecretPin];
//                
//                //NSLog(@"Success:");
//                /*
//                 HomeScreenViewController *homeView = [[HomeScreenViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"HomeScreenViewController"] bundle:nil];
//                 APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:homeView];
//                 APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
//                 [APP_DELEGATE.window makeKeyAndVisible];
//                 */
//                
//                MyGamesViewController *myGamesView = [[MyGamesViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"MyGamesViewController"] bundle:nil];
//                [self.navigationController pushViewController:myGamesView animated:YES];
//            }
//            else
//            {
//                //NSLog(@"Incorrect PIN: Please Re-enter");
//                [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"PIN Mismatched, Please Re-enter" CancelButtonTitle:@"OK" InView:self];
//                _txtPin1.text = @"";
//                _txtPin2.text = @"";
//                _txtPin3.text = @"";
//                _txtPin4.text = @"";
//                _txtUserPin.text = @"";
//            }
//        }
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"Only numeric values are allowed" CancelButtonTitle:@"OK" InView:self];
    }
}

- (IBAction)DidSelectForgotPIN:(id)sender
{
    // Show popup.
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Please Confirm!"
                                                                       message:@"Forgot PIN will erase all your data and then reset your PIN, Do you want to continue.?"
                                                                preferredStyle:(UIAlertControllerStyleAlert)];
        UIAlertAction *alert_yes_action = [UIAlertAction actionWithTitle:@"Continue To Reset PIN"
                                                                   style:(UIAlertActionStyleDefault)
                                           
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                     [self CallResetProcess];
                                                                     
                                                                 }];
        UIAlertAction *alert_no_action = [UIAlertAction actionWithTitle:@"Cancel"
                                                                  style:(UIAlertActionStyleCancel)
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    //
                                                                }];
        [alert addAction:alert_yes_action];
        [alert addAction:alert_no_action];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

-(void)CallResetProcess
{
    [APP_DELEGATE RemoveUserDataAndSetRootViewController];
}

@end
