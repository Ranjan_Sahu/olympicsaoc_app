//
//  TeamAgreementDetailsViewController.m
//  Olympics
//
//  Created by webwerks on 3/22/17.
//  Copyright © 2017 webwerks. All rights reserved.
//

#import "TeamAgreementDetailsViewController.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

CGFloat animatedDistance;
static const CGFloat KEYBOARD_ANIMATION_DURATION = 0.25;
static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 320;
static const CGFloat LANDSCAPE_KEYBOARD_HEIGHT = 162;


@interface TeamAgreementDetailsViewController ()
{
    UITableView *tblViewGrpQueAns;
    AdhocCustomCell *tblCell;
    NSMutableArray *arrayToShowRowData;
    NSMutableDictionary *agreementDetailsDict;
    UIButton *pdfButton, *agreeButton;
    UILabel *webViewSeperator, *horizintalSeperator;
    OLMSingleCheckBox *checkBoxOneObj;
    
    CGFloat controlHeight,rowHeight,tblViewHeight,headerHeight,headertitlelblLeading,subtractedWidth;
    //NSIndexPath *selectedIndex, *previousIndex;
    NSUInteger currSection, prvSection, noOfSection;
    BOOL isSectionSelected, isSectionExpanded, isConsent;
}
@end

@implementation TeamAgreementDetailsViewController
@synthesize incrementedHeight, controlls, contentViewControlls;

- (void)viewDidLoad {
    [super viewDidLoad];
    animatedDistance = 0;
    
    isConsent = [[[NSUserDefaults standardUserDefaults] valueForKey:@"IS_CONSENT_GAME"] boolValue];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    headerHeight = 40.0f;
    headertitlelblLeading = 15.0f;
    subtractedWidth = 25.0f;
    if(IS_IPAD)
    {
        headerHeight = 80.0f;
        headertitlelblLeading = 50.0f;
        subtractedWidth = 40.0f;
    }
    
    _navigatiobBarLogo.text = [NSString stringWithFormat:@"\ue91a"];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.agreementsWebView.delegate = self;
    self.headerWebView.delegate = self;
    
    // Do any additional setup after loading the view.
    if(IS_IPAD)
    {
        _heightViewScrollContent.constant = 1200;
    
        _heightAgreementWebView.constant = 300;
    }
    else if (IS_IPHONE_5)
    {
        _heightViewScrollContent.constant = 900;
        _heightAgreementWebView.constant = 200;
    }
}

-(void)viewWillAppear:(BOOL)animated{
}

-(void)viewDidAppear:(BOOL)animated{
    [self StartAddingControllers];
    [self drawWebviewForDetails];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillLayoutSubviews
{
    _viewScrollContentViewTop.constant = -animatedDistance;
}

#pragma - mark WebService Call

-(void)CallServiceToSaveDataWithDictionary:(NSMutableArray*)recordsArray
{
    [COMMON_SETTINGS showHUD];
    
    //NSString *deviceAddress = [NSString stringWithFormat:@"%@",[self getIPAddress]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:recordsArray forKey:@"lstSingleAgreement"];
    //[parameters setObject:deviceAddress forKey:@"IpAddress"];
    
    if(![[parameters objectForKey:@"lstSingleAgreement"]isKindOfClass:[NSNull class]] &&  [[parameters objectForKey:@"lstSingleAgreement"] count]>0)
    {
        Service *callService = [[Service alloc] init];
        callService.delegate = self;
        [callService requestingURLString:weburl Service:Service_InsertUpdateTeamAgreementDetails withParameters:parameters ContactID:[_playerDetails valueForKey:@"ContactID"]];
        _serviceCalled = Service_Called_Save;
    }
    else
    {
        [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:@"No records to save" CancelButtonTitle:@"OK" InView:self];
    }
    
}
#pragma - mark Service Deleget
-(void)Service_Error:(id)error
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_Error CancelButtonTitle:@"Ok" InView:self];
}

-(void)Service_Success:(NSString *)responseStr
{
    [COMMON_SETTINGS hideHUD];
    
    if(_serviceCalled == Service_Called_Save)
    {
        NSError *error;
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSMutableDictionary *responseDictionery = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
        
        if([[responseDictionery valueForKey:@"Status"]intValue] == 1)
        {
            //Updating isConsent field for userInfo in UD_UserInfo(NSUserDefaults)
//            [[NSUserDefaults standardUserDefaults] setValue:[[responseDictionery valueForKey:@"IsConsent"] isKindOfClass:[NSNull class]]?@"":[responseDictionery valueForKey:@"IsConsent"]forKey:@"IS_CONSENT_GAME"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
            
            //show success alert
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Data Saved Successfully"preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action) {
                
                [self.navigationController popViewControllerAnimated:YES];
                
            }];
            [alert addAction:cancelAction];
            [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:[responseDictionery valueForKey:@"ErrorMessage"] CancelButtonTitle:@"OK" InView:self];
        }
    }
}

-(void)no_Response_Function
{
    [COMMON_SETTINGS hideHUD];
    [COMMON_SETTINGS AlertViewWithTitle:Alert_Title Message:Alert_NoResponse CancelButtonTitle:@"Ok" InView:self];
}

-(void)callMethodToCreateTableView
{
    noOfSection = [self numberOfSectionsInTableView:tblViewGrpQueAns];
    CGFloat initialHeight =  (noOfSection*(headerHeight+5));
    
    if(isSectionExpanded)
        initialHeight = [self callMethodToGetTableHeight];
    
    tblViewGrpQueAns = [[UITableView alloc] initWithFrame:CGRectMake((_viewContent.frame.origin.x + 10),controlHeight,( _viewContent.frame.size.width - 20), initialHeight) style:UITableViewStylePlain];
    
    tblViewGrpQueAns.backgroundColor = [UIColor whiteColor];
    
    [_viewContent addSubview:tblViewGrpQueAns];
    
    [tblViewGrpQueAns registerNib:[UINib nibWithNibName:@"AdhocCustomCell" bundle:nil] forCellReuseIdentifier: @"AdhocCustomCell"];
    
    [tblViewGrpQueAns registerNib:[UINib nibWithNibName:@"AdhocDummyCell" bundle:nil] forCellReuseIdentifier: @"AdhocDummyCell"];
    
    tblViewGrpQueAns.delegate = self;
    tblViewGrpQueAns.dataSource = self;
    
    [tblViewGrpQueAns setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    isSectionSelected = FALSE;
    isSectionExpanded = FALSE;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    controlHeight = controlHeight+tblViewGrpQueAns.frame.size.height+10;
    
    CGFloat heightAfterTblView = 160;
    if (IS_IPAD) {
        heightAfterTblView = 240;
    }
    self.heightViewScrollContent.constant = self.headerWebView.frame.size.height +  self.agreementsWebView.frame.size.height + tblViewGrpQueAns.frame.size.height + heightAfterTblView;
}

#pragma mark - UIWebView Dlegate

//-(void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
////    NSInteger height = [result integerValue];
////    if(height<self.headerWebView.frame.size.height)
////    {
////        _heightHeaderView.constant = height;
////    }
//    [self.view setNeedsDisplay];
//}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return FALSE;
    }
    
    NSString *URL = [[request.URL absoluteString] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if(![URL isEqualToString:@"about:blank"] )
    {
        [[UIApplication sharedApplication] openURL:request.URL];
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

#pragma mark - UIButton Action Implementation

-(void)DidSelectPDFVersion:(UIButton *)sender
{
    //NSLog(@"DidSelectPDFVersion");
    
    if(![[_agreementDict valueForKey:@"AgreementFilePath"] isEqualToString:@""])
    {
        NSString *agreementFilePathString = [_agreementDict valueForKey:@"AgreementFilePath"];
        agreementFilePathString = [agreementFilePathString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        NSURL *URL = [NSURL URLWithString:agreementFilePathString];
        [[UIApplication sharedApplication] openURL:URL];
    }
}

-(void)DidSelectAgree:(UIButton *)sender
{
    [self.view endEditing:YES];
    
    //NSLog(@"DidSelectAgree");
    NSString *value;
    
    if([contentViewControlls count]>0)
    {
        if([[contentViewControlls objectAtIndex:0] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *SingleCheckBoxObj  = (OLMSingleCheckBox*)[contentViewControlls objectAtIndex:0];
            NSDictionary *answersDict = [SingleCheckBoxObj GetAnswerInfo];
            if(answersDict){
                for (id key in answersDict)
                    value = [answersDict objectForKey:key];
            }
        }
    }
    if(value)
    {
        [self checkForMandatoryFieldsInput];
    }
}


#pragma mark - Check for Mandatory Fields Value
-(void)checkForMandatoryFieldsInput{
    [self.view endEditing:YES];
    
        for (int j=0; j<controlls.count; j++) {
            //changes
            NSMutableArray *arrMainAndSubControlls = [[NSMutableArray alloc] initWithArray:[[controlls objectAtIndex:j] valueForKey:@"Controls"]];
            
            for (int k = 0; k<arrMainAndSubControlls.count; k++) {
                
                NSMutableDictionary *subDictMainAndSubControlls = [arrMainAndSubControlls objectAtIndex:k];
                
                if(![[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
                {
                    if([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMRadioButton class]])
                    {
                        OLMRadioButton *RadioButtonObj  = (OLMRadioButton*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [RadioButtonObj GetAnswerInfo];
                        if(!answersDict && RadioButtonObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                    else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMDropDown class]]))
                    {
                        OLMDropDown *DropdownObj  = (OLMDropDown*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [DropdownObj GetAnswerInfo];
                        if(!answersDict && DropdownObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                    else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMCheckBox class]]))
                    {
                        OLMCheckBox *CheckBoxObj  = (OLMCheckBox*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
                        if(!answersDict && CheckBoxObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                    else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMTextField class]]))
                    {
                        OLMTextField *TextFieldObj  = (OLMTextField*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [TextFieldObj GetAnswerInfo];
                        if(!answersDict && TextFieldObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                    else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMTextView class]]))
                    {
                        OLMTextView *TextViewObj  = (OLMTextView*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [TextViewObj GetAnswerInfo];
                        if(!answersDict && TextViewObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                    else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMRadioButtonTextKeyType class]]))
                    {
                        OLMRadioButtonTextKeyType *CheckBoxObj  = (OLMRadioButtonTextKeyType*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [CheckBoxObj GetAnswerInfo];
                        if(!answersDict && CheckBoxObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                    else if (([[subDictMainAndSubControlls objectForKey:@"mainControler"] isKindOfClass:[OLMDatePicker class]]))
                    {
                        OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[subDictMainAndSubControlls objectForKey:@"mainControler"];
                        NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
                        if(!answersDict && DatePickerObj.IsRequired == 1)
                        {
                            [COMMON_SETTINGS AlertViewWithTitle:@"" Message:@"Questions marked with * require a response" CancelButtonTitle:@"Ok" InView:self];
                            return;
                        }
                    }
                }
            }
        }
    NSMutableArray *dataArray = [[NSMutableArray alloc]init];
    [dataArray addObject:_agreementDict];
    
    [self CallServiceToSaveDataWithDictionary:dataArray];
}

#pragma mark - IBAction Implementation
- (IBAction)DidSelectBackButton:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - OLMRadioButton Delegate
-(void)OLMRadioButton:(OLMRadioButton *)radioButton didSelectOptionAtIndex:(NSInteger)index
{
    CGPoint switchPositionPoint = [radioButton convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    if(![[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]isKindOfClass:[NSNull class]] && [[_agreementDict valueForKey:@"lstGroupQuestionAnswer"] count]>0)
    {
        NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
        
        NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
        
        NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
        
        NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
        
        if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
        {
            NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
            
            if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
            {
                if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMRadioButton class]])
                {
                    NSDictionary *answersDict = [radioButton GetAnswerInfo];
                    if(answersDict)
                    {
                        NSString *value;
                        for (id key in answersDict)
                            value = [answersDict objectForKey:key];
                        [dictToEdit setValue:value forKey:@"AHAYesNo"];
                    }
                }
            }
        }
        
        [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
        [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
        [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
        [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    }
}

#pragma mark - OLMCheckBox Delegate
-(void)OLMCheckBox:(OLMCheckBox *)checkBox didSelectOptionAtIndex:(NSInteger)index
{
    [self scrollDown];
    CGPoint switchPositionPoint = [checkBox convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
    
    if(arrayToReplace.count > 0){
        NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
        
        NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
        
        NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
        
        if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
        {
            NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
            
            if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
            {
                if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMCheckBox class]])
                {
                    OLMCheckBox *MultiCheckBoxObj  = (OLMCheckBox*)[currentControlObjects objectForKey:@"mainControler"];
                    
                    NSDictionary *answersDict = [MultiCheckBoxObj GetAnswerInfo];
                    if(answersDict)
                    {
                        if (MultiCheckBoxObj.selectedOptionIdArray.count >0) {
                            NSString *selectedId = [[MultiCheckBoxObj.selectedOptionIdArray valueForKey:@"AHOptionID"] componentsJoinedByString:@","];
                            [dictToEdit setValue:selectedId forKey:@"MultiSelectedOptions"];
                        }
                        else{
                            [dictToEdit setValue:@" " forKey:@"MultiSelectedOptions"];
                        }
                    }
                }
            }
            
            [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
            [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
            [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
            [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
        }
    }
}

#pragma mark - OLMSingleCheckBox Delegate
-(void)OLMSingleCheckBox:(OLMSingleCheckBox*)singleCheckBox didSelectOptionAtIndex:(NSInteger)index
{
    //[self scrollDown];
    CGPoint switchPositionPoint = [singleCheckBox convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    // Handle Agreement checkBox switching
    if([contentViewControlls count]>0)
    {
        if([[contentViewControlls objectAtIndex:0] isKindOfClass:[OLMSingleCheckBox class]])
        {
            OLMSingleCheckBox *SingleCheckBoxObj  = (OLMSingleCheckBox*)[contentViewControlls objectAtIndex:0];
            NSDictionary *answersDict = [SingleCheckBoxObj GetAnswerInfo];
            if(answersDict)
            {
                NSString *value;
                for (id key in answersDict)
                    value = [answersDict objectForKey:key];
                
                [_agreementDict setValue:value forKey:@"chkAgree"];
            }
        }
    }
    
    if(agreeButton.enabled == TRUE){
        agreeButton.enabled = FALSE;
        agreeButton.alpha = 0.7;
    }
    else{
        agreeButton.enabled = TRUE;
        agreeButton.alpha = 1.0;
    }
    
    // Handle SubController CheckBox
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
        
        if(![arrayToReplace isKindOfClass:[NSNull class]]&&[arrayToReplace count]>0)
        {
            NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
            
            NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
            
            NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
            
            NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
            
            if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
            {
                if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMSingleCheckBox class]])
                {
                    OLMSingleCheckBox *DatePickerObj  = (OLMSingleCheckBox*)[currentControlObjects objectForKey:@"mainControler"];
                    NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
                    if(answersDict)
                    {
                        NSString *value;
                        for (id key in answersDict)
                            value = [answersDict objectForKey:key];
                        
                        [dictToEdit setValue:value forKey:@"IsCompleted"];
                    }
                }
            }
            
            if(![[currentControlObjects objectForKey:@"subControlers"] isKindOfClass:[NSNull class]]&&([[currentControlObjects objectForKey:@"subControlers"] count]>0))
            {
                for(int i=0;i<[[currentControlObjects objectForKey:@"subControlers"]count];i++)
                {
                    if([[[currentControlObjects objectForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
                    {
                        OLMSingleCheckBox *DatePickerObj  = (OLMSingleCheckBox*)[[currentControlObjects objectForKey:@"subControlers"] objectAtIndex:i];
                        NSDictionary *answersDict = [DatePickerObj GetAnswerInfo];
                        if(answersDict)
                        {
                            NSString *value;
                            for (id key in answersDict)
                                value = [answersDict objectForKey:key];
                            
                            [dictToEdit setValue:value forKey:@"IsCompleted"];
                        }
                        break;
                    }
                }
            }
            
            [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
            [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
            [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
            [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
        }
    }
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextFieldDidBeginEditing:(UITextField *)textField
{
    [self scrollUp:textField];
}
-(void)OLMTextFieldDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
    
    NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
    
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
        
        if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMTextField class]])
            {
                OLMTextField *TextFieldObj  = (OLMTextField*)[currentControlObjects objectForKey:@"mainControler"];
                
                NSString *value = textField.text;
                if(TextFieldObj.questionTypeId == AHFormatNumeric)
                    [dictToEdit setValue:value forKey:@"AHAValue"];
                else
                    [dictToEdit setValue:value forKey:@"Answer"];
            }
        }
        
        if(![[currentControlObjects objectForKey:@"subControlers"] isKindOfClass:[NSNull class]]&&([[currentControlObjects objectForKey:@"subControlers"] count]>0))
        {
            for(int i=0;i<[[currentControlObjects objectForKey:@"subControlers"]count];i++)
            {
                if([[[currentControlObjects objectForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMTextField class]])
                {
                    NSString *value = textField.text;
                    [dictToEdit setValue:value forKey:@"Note"];
                    break;
                }
            }
        }
        [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
        [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
        [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
        [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    }
}

#pragma mark - OLMTextField Delegate
-(void)OLMTextViewDidBeginEditing:(UITextView *)textView
{
    [self scrollUp:textView];
    
}
-(void)OLMTextViewDidEndEditing:(UITextView*)textView
{
    [self scrollDown];
    CGPoint switchPositionPoint = [textView convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
    
    NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
    
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
        
        if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMTextView class]])
            {
                NSString *value = textView.text;
                [dictToEdit setValue:value forKey:@"Answer"];
            }
        }
        [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
        [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
        [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
        [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    }
}

#pragma mark - OLMDropdown Delegate
-(void)OLMDropdownDidStartEditing:(UITextField*)textField
{
    [self scrollUp:textField];
    
}
-(void)OLMDropdownDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
    
    NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
    
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
        
        if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMDropDown class]])
            {
                OLMDropDown *DropDownObj  = (OLMDropDown*)[currentControlObjects objectForKey:@"mainControler"];
                NSDictionary *answersDict = [DropDownObj GetAnswerInfo];
                if(answersDict)
                {
                    NSString *value;
                    for (id key in answersDict)
                        value = [answersDict objectForKey:key];
                    
                    [dictToEdit setValue:value forKey:@"AHOptionId"];
                }
                else
                {
                    [dictToEdit setValue:@"0" forKey:@"AHOptionId"];
                }
            }
        }
        [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
        [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
        [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
        [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    }
}

-(void)OLMRadioButtonTextKeyType:(OLMRadioButtonTextKeyType*)radioButton didSelectOptionAtIndex:(NSInteger)index
{
}

#pragma mark - OLMDatePicker Delegate
-(void)OLMDatepickerDidStartEditing:(UITextField*)textField
{
    [self scrollUp:textField];
    
}

-(void)OLMDatepickerDidEndEditing:(UITextField*)textField
{
    [self scrollDown];
    CGPoint switchPositionPoint = [textField convertPoint:CGPointZero toView:tblViewGrpQueAns];
    NSIndexPath *indexPath = [tblViewGrpQueAns indexPathForRowAtPoint:switchPositionPoint];
    
    NSMutableArray *arrayToReplace = [[NSMutableArray alloc]initWithArray:[_agreementDict valueForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *rowDataDict = [[NSMutableDictionary alloc]initWithDictionary:[arrayToReplace objectAtIndex:indexPath.section]];
    
    NSMutableArray *arrayToEdit = [[NSMutableArray alloc]initWithArray:[rowDataDict objectForKey:@"lstGroupQuestionAnswer"]];
    
    NSMutableDictionary *dictToEdit = [[NSMutableDictionary alloc]initWithDictionary:[arrayToEdit objectAtIndex:indexPath.row]];
    
    if(![controlls isKindOfClass:[NSNull class]]&&[controlls count]>0)
    {
        NSMutableDictionary *currentControlObjects = [[NSMutableDictionary alloc]initWithDictionary:[[[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"]objectAtIndex:indexPath.row]];
        
        if(![[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            if([[currentControlObjects objectForKey:@"mainControler"] isKindOfClass:[OLMDatePicker class]])
            {
                [dictToEdit setValue:textField.text forKey:@"AHADate"];
            }
        }
        
        if(![[currentControlObjects objectForKey:@"subControlers"] isKindOfClass:[NSNull class]]&&([[currentControlObjects objectForKey:@"subControlers"] count]>0))
        {
            for(int i=0;i<[[currentControlObjects objectForKey:@"subControlers"]count];i++)
            {
                if([[[currentControlObjects objectForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
                {
                    [dictToEdit setValue:textField.text forKey:@"AHADate"];
                    break;
                }
            }
        }
        [arrayToEdit replaceObjectAtIndex:indexPath.row withObject:dictToEdit];
        [rowDataDict setObject:arrayToEdit forKey:@"lstGroupQuestionAnswer"];
        [arrayToReplace replaceObjectAtIndex:indexPath.section withObject:rowDataDict];
        [_agreementDict setObject:arrayToReplace forKey:@"lstGroupQuestionAnswer"];
    }
}

#pragma mark - TableViewDataSource & TableViewDelegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if([controlls count]>0)
        return [controlls count];
    else
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger noOfRows = 1;
    if(isSectionExpanded == TRUE && currSection==section)
    {
        NSMutableArray *dictAgreementQueAns = [controlls objectAtIndex:section];
        noOfRows = [[dictAgreementQueAns valueForKey:@"Controls"] count];
        
    }
    return noOfRows;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat height;
    height = 5;
    
    if(isSectionExpanded == TRUE && currSection==indexPath.section)
    {
        if(controlls.count>0)
        {
            height = [self GetRowHeight: indexPath];
            height = height + 10;
        }
    }
    return height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return headerHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(isSectionExpanded == TRUE && isSectionSelected && currSection==indexPath.section){
        
        tblCell = [tblViewGrpQueAns dequeueReusableCellWithIdentifier:@"AdhocCustomCell" forIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:currSection]];
        
        for (UIView *subview in [tblCell.contentView subviews])
            [subview removeFromSuperview];
        
        if(controlls.count>0)
        {
            NSMutableArray *ControlObjects = [[controlls objectAtIndex:indexPath.section] objectForKey:@"Controls"];
            if(![ControlObjects isKindOfClass:[NSNull class]]&&ControlObjects.count>0)
            {
                NSMutableDictionary *dictMainAndSubObjects = [[NSMutableDictionary alloc] init];
                dictMainAndSubObjects = [ControlObjects objectAtIndex:indexPath.row];
                
                [tblCell.contentView addSubview:[dictMainAndSubObjects objectForKey:@"mainControler"]];
                
                if(![[dictMainAndSubObjects valueForKey:@"subControlers"]isKindOfClass:[NSNull class]]&&[[dictMainAndSubObjects valueForKey:@"subControlers"] count]>0)
                {
                    for(int i=0;i<[[dictMainAndSubObjects valueForKey:@"subControlers"] count];i++)
                    {
                        if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMTextField class]])
                        {
                            OLMTextField *TextFieldObj  = (OLMTextField*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                            
                            [tblCell.contentView addSubview:TextFieldObj];
                        }
                        else  if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
                        {
                            OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                            [tblCell.contentView addSubview:DatePickerObj];
                        }
                        else if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
                        {
                            OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                            [tblCell.contentView addSubview:CheckBoxObj];
                        }
                    }
                }
            }
        }
    }
    else
    {
        tblCell = [tblViewGrpQueAns dequeueReusableCellWithIdentifier:@"AdhocDummyCell" forIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:currSection]];
    }
    
    if (tblCell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AdhocDummyCell" owner:nil options:nil];
        tblCell =  [nib objectAtIndex:0];
        tblCell.backgroundColor = [UIColor clearColor];
    }
    
    return tblCell;
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *viewForHeader = [[UIView alloc] initWithFrame:CGRectMake(tblViewGrpQueAns.frame.origin.x, tblViewGrpQueAns.frame.origin.y, tblViewGrpQueAns.frame.size.width, headerHeight)];
    viewForHeader.tag = section;
    viewForHeader.backgroundColor = [UIColor colorWithRed:0.8980 green:0.8980 blue:0.8980 alpha:1.0];
    
    UILabel *lblHeaderViewTitle = [[UILabel alloc] initWithFrame:CGRectMake(headertitlelblLeading, 0, (viewForHeader.frame.size.width - 70), viewForHeader.frame.size.height)];
    [lblHeaderViewTitle setTextAlignment:NSTextAlignmentLeft];
    [lblHeaderViewTitle setTextColor:[UIColor colorWithRed:0.1373 green:0.4784 blue:0.8235 alpha:1.0]];
    
    UILabel *lblHeaderViewArrowIndicator = [[UILabel alloc] initWithFrame:CGRectMake((viewForHeader.frame.size.width - subtractedWidth - headertitlelblLeading), 0, 30, viewForHeader.frame.size.height)];
    [lblHeaderViewArrowIndicator setTextAlignment:NSTextAlignmentCenter];
    [lblHeaderViewArrowIndicator setTextColor:[UIColor colorWithRed:0.1373 green:0.4784 blue:0.8235 alpha:1.0]];
    
    if(IS_IPAD)
    {
        [lblHeaderViewTitle setFont:app_font32];
        [lblHeaderViewArrowIndicator setFont:[UIFont fontWithName:@"icomoon" size:23]];
    }
    else if (IS_IPHONE)
    {
        [lblHeaderViewTitle setFont:app_font18];
        [lblHeaderViewArrowIndicator setFont:[UIFont fontWithName:@"icomoon" size:13]];
    }
    
    NSString *titleString;
    if(![[_agreementDict objectForKey:@"lstGroupQuestionAnswer"] isKindOfClass:[NSNull class]] && [[_agreementDict objectForKey:@"lstGroupQuestionAnswer"] count]>0)
    {
        NSMutableArray *dictAgreementQueAns = [_agreementDict objectForKey:@"lstGroupQuestionAnswer"];
        NSMutableDictionary *dictGrpQueAns = [dictAgreementQueAns objectAtIndex:section];
        
        titleString = [NSString stringWithFormat:@"%@",[dictGrpQueAns valueForKey:@"GroupHeader"]];
        [lblHeaderViewTitle setText:titleString];
        [lblHeaderViewArrowIndicator setText:@"\ue921"];
        
    }
    
    [viewForHeader addSubview:lblHeaderViewTitle];
    [viewForHeader addSubview:lblHeaderViewArrowIndicator];
    
    UITapGestureRecognizer *tapOnView = [[UITapGestureRecognizer alloc]
                                         initWithTarget:self
                                         action:@selector(DidSelectHeader:) ];
    [viewForHeader addGestureRecognizer:tapOnView];
    
    return viewForHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - Implementation For Tap Gesture Method
-(void)DidSelectHeader:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
    //NSLog(@"selected Section is : %ld",sender.view.tag);
    
    currSection = sender.view.tag;
    NSRange range = NSMakeRange(currSection, 1);
    NSIndexSet *selectedIndexSet = [NSIndexSet indexSetWithIndexesInRange:range];
    
    if(isSectionExpanded == FALSE){
        
        // Expand
        prvSection = currSection;
        isSectionExpanded = TRUE;
        isSectionSelected = TRUE;
        [self SetTableViewHeight];
        [tblViewGrpQueAns reloadSections:selectedIndexSet withRowAnimation:UITableViewRowAnimationFade];
    }
    else if(isSectionExpanded == TRUE){
        
        // Collapse
        if(prvSection == currSection){
            isSectionExpanded =FALSE;
            isSectionSelected = FALSE;
            [self ResetTableViewHeight];
            [tblViewGrpQueAns reloadSections:selectedIndexSet withRowAnimation:UITableViewRowAnimationFade];
        }
        else if(currSection != prvSection){
            
            // Collapse
            isSectionExpanded = FALSE;
            isSectionSelected = FALSE;
            [self ResetTableViewHeight];
            [tblViewGrpQueAns reloadSections:[NSIndexSet indexSetWithIndex:prvSection] withRowAnimation:UITableViewRowAnimationFade];
            prvSection = currSection;
            
            // Expand
            isSectionExpanded = TRUE;
            isSectionSelected = TRUE;
            [self SetTableViewHeight];
            [tblViewGrpQueAns reloadSections:selectedIndexSet withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

-(void)SetTableViewHeight
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromTop
                     animations:^
     {
         CGRect frame = tblViewGrpQueAns.frame;
         frame.size.height = ([self callMethodToGetTableHeight] + (noOfSection*(headerHeight+5*2)));
         tblViewGrpQueAns.frame = frame;
         controlHeight = tblViewGrpQueAns.frame.size.height + tblViewGrpQueAns.frame.origin.y;
         
         CGRect checkBoxFrame = checkBoxOneObj.frame;
         checkBoxFrame.origin.y = controlHeight + 10;
         checkBoxOneObj.frame = checkBoxFrame;
         
         CGRect SeperatorFrame = horizintalSeperator.frame;
         SeperatorFrame.origin.y = controlHeight + 10;
         horizintalSeperator.frame = SeperatorFrame;
         
         CGRect pdfButtonFrame = pdfButton.frame;
         pdfButtonFrame.origin.y = controlHeight + 10;
         pdfButton.frame = pdfButtonFrame;
         
         controlHeight = checkBoxOneObj.frame.size.height + checkBoxOneObj.frame.origin.y;
         
         CGRect agreeButtonFrame = agreeButton.frame;
         agreeButtonFrame.origin.y = controlHeight + 10;
         agreeButton.frame = agreeButtonFrame;
         
         controlHeight = agreeButton.frame.size.height + agreeButton.frame.origin.y;
         
         CGFloat heightAfterTblView = 160;
         if (IS_IPAD) {
             heightAfterTblView = 240;
         }
         
         self.heightViewScrollContent.constant = self.headerWebView.frame.size.height + self.agreementsWebView.frame.size.height + tblViewGrpQueAns.frame.size.height + heightAfterTblView;
         
     }
                     completion:^(BOOL finished)
     {
         //NSLog(@"Completed");
     }];
}

-(void)ResetTableViewHeight
{
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionTransitionFlipFromTop
                     animations:^
     {
         CGRect frame = tblViewGrpQueAns.frame;
         frame.size.height = noOfSection*(headerHeight+5);
         tblViewGrpQueAns.frame = frame;
         controlHeight = tblViewGrpQueAns.frame.size.height + tblViewGrpQueAns.frame.origin.y;
         
         CGRect checkBoxFrame = checkBoxOneObj.frame;
         checkBoxFrame.origin.y = controlHeight + 10;
         checkBoxOneObj.frame = checkBoxFrame;
         
         CGRect SeperatorFrame = horizintalSeperator.frame;
         SeperatorFrame.origin.y = controlHeight + 10;
         horizintalSeperator.frame = SeperatorFrame;
         
         CGRect pdfButtonFrame = pdfButton.frame;
         pdfButtonFrame.origin.y = controlHeight + 10;
         pdfButton.frame = pdfButtonFrame;
         
         controlHeight = checkBoxOneObj.frame.size.height + checkBoxOneObj.frame.origin.y;
         
         CGRect agreeButtonFrame = agreeButton.frame;
         agreeButtonFrame.origin.y = controlHeight + 10;
         agreeButton.frame = agreeButtonFrame;
         
         controlHeight = agreeButton.frame.size.height + agreeButton.frame.origin.y;
         
         CGFloat heightAfterTblView = 160;
         if (IS_IPAD) {
             heightAfterTblView = 240;
         }
         
         self.heightViewScrollContent.constant = self.headerWebView.frame.size.height + self.agreementsWebView.frame.size.height + tblViewGrpQueAns.frame.size.height + heightAfterTblView;
         
     }
                     completion:^(BOOL finished)
     {
         //NSLog(@"Completed");
     }];
}

#pragma mark - Method to add questions on screen
-(void)StartAddingControllers
{
    int yCord = 10;
    NSInteger width = _viewContent.frame.size.width ;
    //CGRect frame = _viewContent.frame;
    
    if(![_agreementDict isKindOfClass:[NSNull class]] && [_agreementDict count] > 0)
    {
        controlls = [[NSMutableArray alloc]init];
        //changes
        NSMutableArray *arrayObject = [_agreementDict objectForKey:@"lstGroupQuestionAnswer"];
        
        for(int sec=0;sec<arrayObject.count;sec++)
        {
            NSMutableArray *arrayGrpQueAns = [[arrayObject objectAtIndex:sec] objectForKey:@"lstGroupQuestionAnswer"];
            NSMutableDictionary *dictControlls = [[NSMutableDictionary alloc]init];
            NSMutableArray *arrayControlls = [[NSMutableArray alloc] init];
            
            for(int row=0;row<arrayGrpQueAns.count;row++)
            {
                NSDictionary *QDictionery = [arrayGrpQueAns objectAtIndex:row];
                int controlTypeId = [[QDictionery valueForKey:@"QuestionFormatId"] intValue];
                
                switch (controlTypeId){
                    case AHFormatText:
                    {
                        NSUInteger YIndex = 0;
                        
                        OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"Answer"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"Answer"] keyboardType:UIKeyboardTypeDefault  delegate:self];
                        
                        controlObj.questionTypeId = AHFormatText;
                        controlObj.tag = row+1;
                        controlObj.isQuestionNeeded = TRUE;
                        //[arrayControlls addObject:controlObj];
                        
                        NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                        [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                        
                        YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                        
                        if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                        {
                            [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:controlObj.frame] forKey:@"subControlers"];
                        }
                        [arrayControlls addObject:controllerDictionary];
                    }
                        break;
                    case AHFormatOption:
                    {
                        NSUInteger YIndex = 0;
                        //Field_DropdownList
                        NSArray *optionsArr = [QDictionery valueForKey:@"ddlAnswerOption"];
                        if(![optionsArr isKindOfClass:[NSNull class]] && optionsArr && [optionsArr count]>0)
                        {
                            NSString *iStr = [QDictionery valueForKey:@"AHOptionId"];
                            int answerInt = 0;
                            if(![iStr isKindOfClass:[NSNull class]])
                            {
                                answerInt = [iStr intValue];
                            }
                            OLMDropDown *controlObj = [[OLMDropDown alloc] initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] Options:optionsArr IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:answerInt delegate:self];
                            controlObj.questionTypeId = AHFormatOption;
                            controlObj.tag = row+1;
                            //[arrayControlls addObject:controlObj];
                            
                            NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                            [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                            
                            YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                            
                            if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                            {
                                [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:controlObj.frame] forKey:@"subControlers"];
                            }
                            [arrayControlls addObject:controllerDictionary];
                        }
                        else
                        {
                            //NSLog(@"Option Array Not Found");
                        }
                    }
                        break;
                    case AHFormatDate:
                    {
                        NSUInteger YIndex = 0;
                        //Field_DatePicker
                        OLMDatePicker *controlObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]   fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"AHADate"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"AHADate"]  delegate:self];
                        controlObj.questionTypeId = AHFormatDate;
                        controlObj.tag = row+1;
                        //[arrayControlls addObject:controlObj];
                        
                        NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                        [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                        
                        YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                        
                        if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                        {
                            [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:controlObj.frame] forKey:@"subControlers"];
                        }
                        [arrayControlls addObject:controllerDictionary];
                    }
                        break;
                    case AHFormatNumeric:
                    {
                        NSUInteger YIndex = 0;
                        //Field_NumericTextField
                        OLMTextField *controlObj = [[OLMTextField alloc] initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"AHAValue"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"AHAValue"] keyboardType:UIKeyboardTypeDecimalPad  delegate:self];
                        
                        controlObj.questionTypeId = AHFormatNumeric;
                        controlObj.tag = row+1;
                        //[arrayControlls addObject:controlObj];
                        
                        NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                        [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                        
                        YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                        
                        if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                        {
                            [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:controlObj.frame] forKey:@"subControlers"];
                        }
                        [arrayControlls addObject:controllerDictionary];
                    }
                        break;
                    case AHFormatYesNo:
                    {
                        NSUInteger YIndex = 0;
                        //OLM CheckBox
                        NSArray *optionArray = [QDictionery valueForKey:@"ddlAnswerOption"];
                        if(![optionArray isKindOfClass:[NSNull class]] && optionArray && [optionArray count]>0)
                        {
                            NSString *answer = [QDictionery valueForKey:@"AHAYesNo"];
                            BOOL answerbool = FALSE;
                            if(![answer isKindOfClass:[NSNull class]])
                            {
                                answerbool = [answer boolValue];
                            }
                            OLMRadioButton *radioButtonObj = [[OLMRadioButton alloc] initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"] fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] options:optionArray IsRequired:NO defaultValue:answerbool delegate:self];
                            radioButtonObj.questionTypeId = AHFormatYesNo;
                            radioButtonObj.tag = row+1;
                            //[arrayControlls addObject:radioButtonObj];
                            
                            NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                            [controllerDictionary setObject:radioButtonObj forKey:@"mainControler"];
                            
                            YIndex = radioButtonObj.frame.origin.y + radioButtonObj.frame.size.height;
                            
                            if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                            {
                                [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:radioButtonObj.frame] forKey:@"subControlers"];
                            }
                            [arrayControlls addObject:controllerDictionary];
                        }
                    }
                        break;
                    case AHFormatTextMultiline:
                    {
                        NSUInteger YIndex = 0;
                        //Field_TextField
                        OLMTextView *controlObj = [[OLMTextView alloc] initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"]  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:[[QDictionery valueForKey:@"IsRequired"] intValue] defaultValue:[[QDictionery valueForKey:@"Answer"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"Answer"] keyboardType:UIKeyboardTypeDefault  delegate:self];
                        
                        controlObj.questionTypeId = AHFormatTextMultiline;
                        controlObj.tag = row+1;
                        //[arrayControlls addObject:controlObj];
                        
                        NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                        [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                        
                        YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                        
                        if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                        {
                            [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:controlObj.frame] forKey:@"subControlers"];
                        }
                        [arrayControlls addObject:controllerDictionary];
                    }
                        break;
                    case AHFormatMulti:
                    {
                        NSUInteger YIndex = 0;
                        
                        //Field_MultipleCheckBox
                        NSArray *optionArray = [[NSArray alloc]init];
                        
                        if ([[QDictionery valueForKey:@"IsMultiSelected"] boolValue] == TRUE) {
                            optionArray = [QDictionery valueForKey:@"lstMultiChoiceOption"];
                        }
                        else {
                            optionArray = [QDictionery valueForKey:@"ddlAnswerOption"];
                        }
                        
                        if(![optionArray isKindOfClass:[NSNull class]] && optionArray && [optionArray count]>0)
                        {
                            NSString *iStr = [QDictionery valueForKey:@"Answer"];
                            int answerInt = 0;
                            if(![iStr isKindOfClass:[NSNull class]])
                            {
                                answerInt = [iStr intValue];
                            }
                            
                            OLMCheckBox *controlObj = [[OLMCheckBox alloc]initWithFrame:CGRectMake(10, yCord, width-40, 1) Question:[QDictionery valueForKey:@"QuestionHeader"] fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] options:optionArray IsRequired:NO defaultValue:answerInt delegate:self];
                            
                            controlObj.questionTypeId = AHFormatMulti;
                            controlObj.tag = row+1;
                            //[arrayControlls addObject:controlObj];
                            
                            NSMutableDictionary *controllerDictionary = [[NSMutableDictionary alloc] init];
                            [controllerDictionary setObject:controlObj forKey:@"mainControler"];
                            
                            YIndex = controlObj.frame.origin.y + controlObj.frame.size.height;
                            
                            if([[QDictionery valueForKey:@"IncludeNote"] intValue]||[[QDictionery valueForKey:@"IncludeDate"] intValue]||[[QDictionery valueForKey:@"IncludeCompleted"] intValue])
                            {
                                [controllerDictionary setObject:[self callMethodToAddSubControllers:QDictionery andYIndex:YIndex andFrame:controlObj.frame] forKey:@"subControlers"];
                            }
                            [arrayControlls addObject:controllerDictionary];
                        }
                    }
                        break;
                    default:
                        break;
                }
            }
            [dictControlls setValue:[NSString stringWithFormat:@"%d",sec] forKey:@"SectionId"];
            [dictControlls setObject:arrayControlls forKey:@"Controls"];
            [controlls addObject:dictControlls];
        }
    }
}

#pragma mark - Method To Calculate Dynamic Height of Row
-(CGFloat)GetRowHeight: (NSIndexPath *)currentIndex
{
    NSMutableArray *ControlObjects = [[controlls objectAtIndex:currentIndex.section] objectForKey:@"Controls"];
    
    if([ControlObjects count]>0)
    {
        NSMutableDictionary *dictMainAndSubObjects = [[NSMutableDictionary alloc] init];
        
        dictMainAndSubObjects = [ControlObjects objectAtIndex:currentIndex.row];
        
        if(![[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[NSNull class]])
        {
            // Calculate Height for Sub Controllers
            CGFloat height = 0;
            if(![[dictMainAndSubObjects valueForKey:@"subControlers"]isKindOfClass:[NSNull class]]&&[[dictMainAndSubObjects valueForKey:@"subControlers"] count]>0)
            {
                for(int i=0;i<[[dictMainAndSubObjects valueForKey:@"subControlers"] count];i++)
                {
                    if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMTextField class]])
                    {
                        OLMTextField *TextFieldObj  = (OLMTextField*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                        height = height + TextFieldObj.frame.size.height;
                    }
                    else  if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMDatePicker class]])
                    {
                        OLMDatePicker *DatePickerObj  = (OLMDatePicker*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                        height = height + DatePickerObj.frame.size.height;
                    }
                    else if([[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i] isKindOfClass:[OLMSingleCheckBox class]])
                    {
                        OLMSingleCheckBox *CheckBoxObj  = (OLMSingleCheckBox*)[[dictMainAndSubObjects valueForKey:@"subControlers"] objectAtIndex:i];
                        height = height + CheckBoxObj.frame.size.height;
                    }
                }
            }
            
            // Calculate Height for Main Controllers
            if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMRadioButton class]])
            {
                OLMRadioButton *RadioButtonObj = (OLMRadioButton*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + RadioButtonObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMDropDown class]])
            {
                OLMDropDown *DropdownObj = (OLMDropDown*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + DropdownObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMCheckBox class]])
            {
                OLMCheckBox *CheckBoxObj = (OLMCheckBox*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + CheckBoxObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMTextField class]])
            {
                OLMTextField *TextFieldObj = (OLMTextField*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + TextFieldObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMRadioButtonTextKeyType class]])
            {
                OLMRadioButtonTextKeyType *CheckBoxObj = (OLMRadioButtonTextKeyType*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + CheckBoxObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMDatePicker class]])
            {
                OLMDatePicker *DatePickerObj = (OLMDatePicker*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + DatePickerObj.frame.size.height;
            }
            else if([[dictMainAndSubObjects valueForKey:@"mainControler"] isKindOfClass:[OLMTextView class]])
            {
                OLMTextView *TextViewObj = (OLMTextView*)[dictMainAndSubObjects valueForKey:@"mainControler"];
                height = height + TextViewObj.frame.size.height;
            }
            return height;
        }
    }
    return 10.0f;
}

#pragma mark - Method To Calculate Dynamic Height of TableView
-(CGFloat)callMethodToGetTableHeight
{
    CGFloat expandedHeight = 0;
    for(int i=0;i<[[[controlls objectAtIndex:currSection] objectForKey:@"Controls"] count];i++)
    {
        expandedHeight = expandedHeight + [self GetRowHeight:[NSIndexPath indexPathForRow:i inSection:currSection]];
        ;
    }
    
    if(isSectionExpanded)
        return expandedHeight;
    else
        return (headerHeight+5);
}

#pragma mark - Add Sub Controllers
-(NSMutableArray* )callMethodToAddSubControllers:(NSDictionary *)QDictionery andYIndex :(NSUInteger)YIndex andFrame :(CGRect)frame
{
    int IncludeNote = [[QDictionery valueForKey:@"IncludeNote"] intValue];
    int IncludeDate = [[QDictionery valueForKey:@"IncludeDate"] intValue];
    int IncludeCompleted = [[QDictionery valueForKey:@"IncludeCompleted"] intValue];
    
    NSMutableArray *subControlles = [[NSMutableArray alloc] init];
    
    if(IncludeNote)
    {
        OLMTextField *subcontrolObj = [[OLMTextField alloc] initWithFrame:CGRectMake(frame.origin.x+10, YIndex, frame.size.width-20, 1) Question:@"Notes"  fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:FALSE defaultValue:[[QDictionery valueForKey:@"Note"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"Note"] keyboardType:UIKeyboardTypeDefault  delegate:self];
        
        subcontrolObj.questionTypeId = AHFormatText;
        subcontrolObj.tag = 777;
        [subControlles addObject:subcontrolObj];
        
        YIndex = YIndex + subcontrolObj.frame.size.height;
    }
    if(IncludeDate)
    {
        OLMDatePicker *subcontrolObj = [[OLMDatePicker alloc] initWithFrame:CGRectMake(frame.origin.x+10, YIndex, frame.size.width-20, 1) Question:@"Date" fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] IsRequired:FALSE defaultValue:[[QDictionery valueForKey:@"AHADate"] isKindOfClass:[NSNull class]]?@"":[QDictionery valueForKey:@"AHADate"]  delegate:self];
        
        subcontrolObj.questionTypeId = AHFormatDate;
        subcontrolObj.tag = 888;
        [subControlles addObject:subcontrolObj];
        
        YIndex = YIndex + subcontrolObj.frame.size.height;
    }
    if(IncludeCompleted)
    {
        OLMSingleCheckBox *subcontrolObj = [[OLMSingleCheckBox alloc]initWithFrame:CGRectMake(frame.origin.x+10, YIndex, frame.size.width - 20, 1) questionId:[[QDictionery valueForKey:@"QuestionId"] intValue] fieldIdName:[QDictionery valueForKey:@"AHAnswerID"] OptionTitle:@"Completed" isRequired:FALSE defaultSelected:[[QDictionery valueForKey:@"IsCompleted"] boolValue] delegate:self];
        
        subcontrolObj.questionTypeId = AHFormatSingleCheckBox;
        subcontrolObj.tag = 999;
        [subControlles addObject:subcontrolObj];
        
        YIndex = YIndex + subcontrolObj.frame.size.height;
    }
    return subControlles;
}

#pragma mark - Access Device IP Address
- (NSString *)getIPAddress {
    
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            if(temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    return address;
}
#pragma mark - View Scrolling

-(void)scrollUp:(id)sender
{
    CGRect textFieldRect;
    UITextField *temptextView = sender;
    textFieldRect = [self.viewContent.window convertRect:temptextView.bounds fromView:temptextView];
    CGRect viewRect = [self.viewContent.window convertRect:self.viewContent.bounds fromView:self.viewContent];
    
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator = midline - viewRect.origin.y - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.viewContent.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    [self.viewContent setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)scrollDown {
    
    CGRect viewFrame = self.viewContent.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.viewContent setFrame:viewFrame];
    [UIView commitAnimations];
}

-(void)drawWebviewForDetails{
    
    [self.view endEditing:YES];
    isSectionExpanded = FALSE;
    
    animatedDistance = 0;
    currSection = 0;
    contentViewControlls = [[NSMutableArray alloc] init];
    
    controlHeight = _agreementsWebView.frame.origin.y + _agreementsWebView.frame.size.height;
    
    [checkBoxOneObj removeFromSuperview];
    [pdfButton removeFromSuperview];
    [agreeButton removeFromSuperview];
    [tblViewGrpQueAns removeFromSuperview];
    [webViewSeperator removeFromSuperview];
    [horizintalSeperator removeFromSuperview];
    
    [self.viewContent layoutIfNeeded];
    
    self.titleLabel.text = [_agreementDict valueForKey:@"AgreementTitle"];
    
    NSString *heaserHtmlString = [_agreementHeader isKindOfClass:[NSNull class]]?@"":_agreementHeader;
    [_headerWebView loadHTMLString:heaserHtmlString baseURL:nil];

    
    if(![[_agreementDict valueForKey:@"AgreementText"] isEqualToString:@""])
    {
        NSString *agreementHTMlString = [_agreementDict valueForKey:@"AgreementText"];
        [_agreementsWebView loadHTMLString:agreementHTMlString baseURL:nil];
    }
    
    //UILabel
    webViewSeperator = [[UILabel alloc]initWithFrame:CGRectMake(_viewContent.frame.origin.x, controlHeight, _viewContent.frame.size.width, 1.0)];
    [webViewSeperator setBackgroundColor:[UIColor grayColor]];
    [self.viewContent addSubview:webViewSeperator];
    controlHeight = controlHeight+webViewSeperator.frame.size.height+10;
    
    // Create Table View
    if(([[_agreementDict valueForKey:@"lstGroupQuestionAnswer"] count]>0))
    {
        // Table View
        [self callMethodToCreateTableView];
    }
    
    CGFloat ButtonWidth = 40;
    CGFloat ButtonHeight = 40;
    if(IS_IPAD)
    {
        ButtonWidth = 60;
        ButtonHeight = 60;
    }
    
    // OLM Single Check Box
    
    if(IS_IPHONE_5)
    {
        checkBoxOneObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(_viewContent.frame.origin.x+10, controlHeight, 320-50-ButtonWidth, 1) questionId:102 fieldIdName:@"chkWheelChair" OptionTitle:@"I confirm that I have read and understood the above and agree to its terms" isRequired:NO defaultSelected:[_agreementResponseDict[@"chkAgree"] boolValue] delegate:self];
    }
    else
    {
        checkBoxOneObj = [[OLMSingleCheckBox alloc] initWithFrame:CGRectMake(_viewContent.frame.origin.x+10, controlHeight, _viewContent.frame.size.width-30-ButtonWidth, 1) questionId:102 fieldIdName:@"chkWheelChair" OptionTitle:@"I confirm that I have read and understood the above and agree to its terms" isRequired:NO defaultSelected:[_agreementResponseDict[@"chkAgree"] boolValue] delegate:self];
    }
    
    checkBoxOneObj.questionTypeId = Field_SingleCheckBox;
    checkBoxOneObj.tag = 1;
    [contentViewControlls addObject:checkBoxOneObj];
    [self.viewContent addSubview:checkBoxOneObj];
    
    // To Disable checkBox for User if Already Checked
    if([_agreementResponseDict[@"chkAgree"] boolValue])
    {
        checkBoxOneObj.clickableButton.enabled = FALSE;
    }
    
    if (!([[_agreementDict valueForKey:@"AgreementFile"] isEqualToString:@""])){
        
        //UILabel
        if(IS_IPHONE_5)
        {
            horizintalSeperator = [[UILabel alloc]initWithFrame:CGRectMake((checkBoxOneObj.frame.origin.x + checkBoxOneObj.frame.size.width + 15), controlHeight, 1.0, ButtonHeight)];
        }
        else
        {
            horizintalSeperator = [[UILabel alloc]initWithFrame:CGRectMake((checkBoxOneObj.frame.origin.x + checkBoxOneObj.frame.size.width +(checkBoxOneObj.frame.origin.x/2)), controlHeight, 1.0, ButtonHeight)];
        }
        [horizintalSeperator setBackgroundColor:[UIColor lightGrayColor]];
        horizintalSeperator.alpha =0.7;
        [self.viewContent addSubview:horizintalSeperator];
        
        // UIButton
        if(IS_IPHONE_5)
        {
            pdfButton = [[UIButton alloc] initWithFrame:CGRectMake((horizintalSeperator.frame.origin.x + (horizintalSeperator.frame.size.width) + 15),controlHeight,ButtonHeight,ButtonHeight)];
        }
        else
        {
            pdfButton = [[UIButton alloc] initWithFrame:CGRectMake((horizintalSeperator.frame.origin.x + horizintalSeperator.frame.size.width+(checkBoxOneObj.frame.origin.x/2)),controlHeight,ButtonHeight,ButtonHeight)];
        }
        
        if(IS_IPAD)
        {
            [pdfButton.titleLabel setFont:app_font21];
            [pdfButton setImage:[UIImage imageNamed:@"PDF_box"] forState:UIControlStateNormal];
            pdfButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        else if(IS_IPHONE)
        {
            [pdfButton.titleLabel setFont:app_font13];
            [pdfButton setImage:[UIImage imageNamed:@"PDF_box"] forState:UIControlStateNormal];
            pdfButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
        }
        
        [pdfButton setBackgroundColor: [UIColor colorWithRed:0.0000 green:0.5255 blue:0.8314 alpha:1.0]];
        pdfButton.layer.shadowOffset = CGSizeMake(0.5,0.5);
        pdfButton.layer.shadowColor = [UIColor blackColor].CGColor;
        pdfButton.layer.shadowOpacity = 0.3;
        pdfButton.layer.shadowRadius = 1;
        [pdfButton addTarget:self action:@selector(DidSelectPDFVersion:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.viewContent addSubview:pdfButton];
    }
    controlHeight = controlHeight+checkBoxOneObj.frame.size.height+10;
    
    // UIButton
    agreeButton = [[UIButton alloc] initWithFrame:CGRectMake((_viewContent.frame.origin.x + 10),controlHeight,(_viewContent.frame.size.width - 20),headerHeight)];
    
    [agreeButton.titleLabel setFont:app_font20];
    if(IS_IPAD)
        [agreeButton.titleLabel setFont:app_font27];
    
    [agreeButton setBackgroundColor: [UIColor colorWithRed:0.1020 green:0.3882 blue:0.6706 alpha:1.0]];
    [agreeButton setTitle:@"Agree" forState:UIControlStateNormal];
    [agreeButton setTitleColor:color_white forState:UIControlStateNormal];
    agreeButton.layer.shadowOffset = CGSizeMake(0.5,0.5);
    agreeButton.layer.shadowColor = [UIColor blackColor].CGColor;
    agreeButton.layer.shadowOpacity = 0.3;
    agreeButton.layer.shadowRadius = 1;
    [agreeButton addTarget:self action:@selector(DidSelectAgree:) forControlEvents:UIControlEventTouchUpInside];
    agreeButton.enabled = FALSE;
    agreeButton.alpha = 0.7;
    
    [self.viewContent addSubview:agreeButton];
   // [_viewScrollContent layoutIfNeeded];
    controlHeight = controlHeight+agreeButton.frame.size.height+10;
    
    // Disable AgreeButton if isConsent is ON
    if(isConsent)
    {
        agreeButton.enabled = FALSE;
        agreeButton.alpha = 0.7;
    }
    else
    {
        agreeButton.enabled = TRUE;
        agreeButton.alpha = 1.0;
    }
    
    if([_agreementDict[@"chkAgree"] boolValue])
    {
        agreeButton.enabled = TRUE;
        agreeButton.alpha = 1.0;
    }
    else
    {
        agreeButton.enabled = FALSE;
        agreeButton.alpha = 0.7;
    }
}

@end
