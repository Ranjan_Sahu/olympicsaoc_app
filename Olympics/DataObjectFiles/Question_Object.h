//
//  Question_Object.h
//  JTI
//
//  Created by webwerks on 10/22/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Question_Object : NSObject

@property (nonatomic,assign) NSInteger FieldSectionId;
@property (nonatomic,assign) NSInteger ControlTypeId;
@property (nonatomic,assign) NSInteger LinkOrder;
@property (nonatomic,assign) BOOL Visibility;
@property (nonatomic,assign) BOOL IsRequired;
@property(nonatomic,retain) NSArray *DropdownData;
@property (nonatomic,assign) NSInteger SelectedItemValue;
@property (nonatomic,retain) NSString *textData;
@property (nonatomic,retain) NSString *strRadioItem;
@property (nonatomic,retain) NSString *HandedSelected;
@end
