
#import <Foundation/Foundation.h>


@protocol Service_delegate <NSObject>
-(void)Service_Success:(NSString *)responseStr;
-(void)Service_Error:(id)error;
-(void)no_Response_Function;

@end



@interface Service : NSObject
{
    
    id <Service_delegate> delegate;
}
@property(nonatomic,retain) id <Service_delegate> delegate;
-(void)requestingURLString:(NSString*)URLString Service:(NSString*)ServiceName withParameters:(NSMutableDictionary *)param ContactID:(NSString*)ContactID;
-(void)requestingNonUserInfoURLString:(NSString*)URLString Service:(NSString*)ServiceName withParameters:(NSMutableDictionary *)param;

-(void)requestingURLString:(NSString*)URLString Service:(NSString*)ServiceName withParameters:(NSMutableDictionary *)param ContactID:(NSString*)ContactID imagesDict:(NSDictionary *)imgDict;


@end
