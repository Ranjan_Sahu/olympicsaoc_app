//
//  AppDelegate.m
//  Olympics
//
//  Created by webwerks on 05/07/16.
//  Copyright © 2016 webwerks. All rights reserved.
//

#import "AppDelegate.h"
//#import <GooglePlus/GooglePlus.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "Constant.h"

//#import <linkedin-sdk/LISDK.h>

@interface AppDelegate ()
{
    Reachability *internetReachable;
}

@end

@implementation AppDelegate
static NSString * const keyClientIdGoogle = @"75420297132-2ab4shq8mcdi6huhgqloikliqekuvnr9.apps.googleusercontent.com";

@synthesize window,PinController,loginController,navController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability| UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }

//    for (NSString *familyName in [UIFont familyNames]){
        //NSLog(@"Family name: %@", familyName);
//        for (NSString *fontName in [UIFont fontNamesForFamilyName:familyName]) {
            //NSLog(@"--Font name: %@", fontName);
//        }
//    }
//    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    [Fabric with:@[[Crashlytics class]]];

    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    
    // FBLogin
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //if(userInfo)
    if([[[NSUserDefaults standardUserDefaults] valueForKey:UD_UserSecretPin] length] == 0)
    {
        loginController = [[loginViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"LoginViewController"] bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:loginController];
        self.window.rootViewController = navController;
        
    }
    else
    {
        PinController = [[EnterPinViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"EnterPinViewController"] bundle:nil];
        navController = [[UINavigationController alloc] initWithRootViewController:PinController];
        self.window.rootViewController = navController;
    }
    
//    UIImage *navbar = [UIImage imageNamed:@"navbar.png"];
//    [[UINavigationBar appearance] setBackgroundImage:navbar forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
//    [[UINavigationBar appearance] setTranslucent:NO];
//
//    NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
//    [[UINavigationBar appearance] setTitleTextAttributes:textTitleOptions];
    
    [window makeKeyAndVisible];
    
    // Internet connection changed notificarion registration
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                        selector:@selector(changedNetworkStatus:)
                                                 name:kReachabilityChangedNotification object:nil];
    internetReachable = [Reachability reachabilityForInternetConnection] ;
    [internetReachable startNotifier];
    
    
    //Post notification to start the background processing
    [[NSNotificationCenter defaultCenter] postNotificationName:kReachabilityChangedNotification object:nil];
    
    [GIDSignIn sharedInstance].clientID = keyClientIdGoogle;
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    
    NSLog(@"My token is: %@", deviceToken);
    NSString * deviceTokenString1 = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""]   stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"the generated device token string is : %@",deviceTokenString1);
    
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString1 forKey:Device_Token];
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}



- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Internet notification method

- (void)changedNetworkStatus:(NSNotification *)notice {
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            [self SetNetworkStatus:FALSE];
            //NSLog(@"The internet is down.");
            break;
        }
        default:
        {
            break;
        }
    }
}

#pragma mark - Other method
-(void)RemoveUserDataAndSetRootViewController
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_UserSecretPin];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UD_UserInfo];
    [[NSUserDefaults standardUserDefaults] synchronize];
    loginController = [[loginViewController alloc] initWithNibName:[COMMON_SETTINGS requiredScreenForName:@"LoginViewController"] bundle:nil];
    navController = [[UINavigationController alloc] initWithRootViewController:loginController];
    self.window.rootViewController = navController;
    [window makeKeyAndVisible];
}

#pragma mark -  No connectivity method
-(void)SetNetworkStatus:(BOOL)status
{
    [UIView animateWithDuration:0.3 animations:^(void)
     {
         if(!status)
         {
             _NoInternetController = [[NoInternetAccessViewController alloc] initWithNibName:@"NoInternetAccessViewController" bundle:nil];
             [[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:_NoInternetController animated:YES completion:nil];
         }
     }];
}

-(void)SetLeftPaddingFor:(UITextField*)textfield width:(int)width imageIfAny:(UIImage*)image
{
    UIImageView *imgArrow = [[UIImageView alloc] init];
    if(image)
        imgArrow.image = image;
    imgArrow.frame = CGRectMake(0.0, 6.0, width, textfield.frame.size.height-12);
    imgArrow.contentMode = UIViewContentModeScaleAspectFit;
    
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = imgArrow;
}

-(void)SetRightPaddingFor:(UITextField*)textfield width:(int)width imageIfAny:(UIImage*)image
{
    UIImageView *imgArrow = [[UIImageView alloc] init];
    if(image)
        imgArrow.image = image;
    imgArrow.frame = CGRectMake(0.0, 6.0, width, textfield.frame.size.height-12);
    imgArrow.contentMode = UIViewContentModeScaleAspectFit;
    
    textfield.rightViewMode = UITextFieldViewModeAlways;
    textfield.rightView = imgArrow;
}


#pragma - mark Social SignIn
- (BOOL)application: (UIApplication *)application openURL: (NSURL *)url sourceApplication: (NSString *)sourceApplication annotation: (id)annotation {
    
    //NSLog(@"SCHEMES ### : %@",[url scheme]);
    
    if ([[GIDSignIn sharedInstance] handleURL:url                                   sourceApplication:sourceApplication
                                   annotation:annotation])
    {
        return [[GIDSignIn sharedInstance] handleURL:url                                   sourceApplication:sourceApplication
                                          annotation:annotation];
    }
    else if([[FBSDKApplicationDelegate sharedInstance] application:application
                                                           openURL:url
                                                 sourceApplication:sourceApplication
                                                        annotation:annotation])
    {
        return YES;
    }
    return NO;
    
}

- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary *)options {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}


+ (BOOL)allowsAnyHTTPSCertificateForHost:(NSString *)host {
    return YES;
}


@end
