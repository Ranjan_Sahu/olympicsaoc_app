//
//  UITextFieldView.m
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMTextView.h"

@implementation OLMTextView
@synthesize increamentedHeight,delegate,textViewView,questionLbl;
-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired defaultValue:(NSString*)defaultValue keyboardType:(UIKeyboardType)keyboard delegate:(id)sender
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    
    if (self)
    {
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        
        questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        
        questionLbl.textColor = color_blueFont;

        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:font}
                                                 context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.frame = CGRectMake(5 , increamentedHeight - 5, self.frame.size.width, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height + 3;
        questionLbl.text = question;
        
        [self addSubview:questionLbl];
        
        if(IS_IPAD)
        {
            textViewView = [[UITextView alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width - self.frame.origin.x, 90)];
            [textViewView setFont:app_font24];
            increamentedHeight = increamentedHeight + 95;

        }
        else
        {
            textViewView = [[UITextView alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width - self.frame.origin.x, 55)];
            [textViewView setFont:app_font13];
            increamentedHeight = increamentedHeight + 55;
        }
        CALayer *imageLayer = textViewView.layer;
//        [imageLayer setCornerRadius:10];
        [imageLayer setBorderWidth:1];
        imageLayer.borderColor=[[UIColor lightGrayColor] CGColor];
        
        textViewView.backgroundColor = [UIColor clearColor];
        textViewView.textColor = color_gray;
        textViewView.textAlignment = NSTextAlignmentLeft;
        textViewView.text = [NSString stringWithFormat:@"%@",defaultValue];
//        if([textViewView.text length] == 0)
//        {
//            questionLbl.hidden = TRUE;
//        }
        [self addSubview:textViewView];
        textViewView.delegate = self;
        textViewView.keyboardType = keyboard;
//        [self SetLeftPaddingFor:textViewView];
        
        UIToolbar *DoneButtonToolbar = [[UIToolbar alloc] init];
        [DoneButtonToolbar sizeToFit];
        DoneButtonToolbar.tintColor = [UIColor colorWithRed:0.1176 green:0.0902 blue:0.2549 alpha:1.0f];
        UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleDone target:self
                                                                      action:@selector(ResignFirstResponderTextView)];
        doneButton.tag = 5;
        [DoneButtonToolbar setItems:[NSArray arrayWithObjects:flexibleItem,doneButton, nil]];
        textViewView.inputAccessoryView = DoneButtonToolbar;
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight + 10, self.frame.size.width, 1)];
        [line setBackgroundColor:color_lightGray];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 15;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            mandetoryLabel.textColor = [UIColor blackColor];
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(textViewView.frame.origin.x - 5 + textViewView.frame.size.width, textViewView.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];
            }
            else
            {
                mandetoryLabel.frame = CGRectMake(textViewView.frame.origin.x - 18 + textViewView.frame.size.width, textViewView.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];
            }
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
        }
    }
    return self;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
   if(self.questionTypeId == Field_NumericTextField)
   {
       [[NSOperationQueue mainQueue] addOperationWithBlock:^{
           [[UIMenuController sharedMenuController] setMenuVisible:NO animated:NO];
       }];
   }
    return NO;
}

-(void)ResignFirstResponderTextView
{
    [textViewView resignFirstResponder];
}


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [delegate OLMTextViewDidBeginEditing:textView];
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
     [delegate OLMTextViewDidEndEditing:textView];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *textString = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
    if ([[textString stringByTrimmingCharactersInSet: set] length] == 0)
    {
        if([textString length] == 0)
            return true;
        else
            return false;
    }
    else
    {
        if([textString length] > 0)
        {
            questionLbl.hidden = false;
        }
        else
        {
            questionLbl.hidden = true;
        }
        return true;
    }
}

-(NSDictionary*)GetAnswerInfo
{
    if(_IsRequired && [textViewView.text length] == 0)
    {
        return nil;
    }
        
    NSDictionary *response = @{ [contollerInfo valueForKey:@"FieldIdName"]:textViewView.text };
    return response;
    
}

-(void)SetLeftPaddingFor:(UITextField*)textfield
{
    UIImageView *imgArrow = [[UIImageView alloc] init];
    imgArrow.frame = CGRectMake(0.0, 0.0, 10.0, textfield.frame.size.height);
    imgArrow.contentMode = UIViewContentModeCenter;
    
    textfield.leftViewMode = UITextFieldViewModeAlways;
    textfield.leftView = imgArrow;
}


@end
