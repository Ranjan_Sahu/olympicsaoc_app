//
//  OLMImagePickerView.h
//  Sales
//
//  Created by Vikas on 06/08/15.
//  Copyright (c) 2015 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

@class OLMImagePickerView;
@protocol OLMImagePickerViewDelegate<NSObject>
@required
//-(void)OLMImagePickerDidSelect:(UIButton*)Sender ForView:(UIImageView*)imageView;
-(void)OLMImagePickerDidDismissWithImage:(UIImage*) image forImageView:(UIImageView*)imageView;
@end



@interface OLMImagePickerView : UIView<UIImagePickerControllerDelegate,UIActionSheetDelegate>
{
    NSMutableDictionary *contollerInfo;
}
@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property(nonatomic,retain) UIImageView *imageView;
@property(nonatomic,retain) UIImage *imageForView;
@property(nonatomic,retain) UIButton *button;
@property (nonatomic) NSInteger questionTypeId;
@property (weak)id<OLMImagePickerViewDelegate>delegate;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired delegate:(id)sender;
-(NSDictionary*)GetAnswerInfo;
@end