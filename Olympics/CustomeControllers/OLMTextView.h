//
//  OLMTextView
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OLMTextView;
@protocol OLMTextViewDelegate<NSObject>
@required
-(void)OLMTextViewDidBeginEditing:(UITextView*)textView;
-(void)OLMTextViewDidEndEditing:(UITextView*)textView;
@end


@interface OLMTextView : UIView<UITextViewDelegate>
{
        NSMutableDictionary *contollerInfo;
}
@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property (nonatomic) NSInteger questionTypeId;
@property(nonatomic,retain) UITextView *textViewView;
@property(nonatomic,retain) UILabel *questionLbl;
@property (weak)id<OLMTextViewDelegate>delegate;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired defaultValue:(NSString*)defaultValue keyboardType:(UIKeyboardType)keyboard delegate:(id)sender;
-(NSDictionary*)GetAnswerInfo;
@end
