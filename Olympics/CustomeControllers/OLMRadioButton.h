//
//  UIRadioButton.h
//  MIRadioButtonGroup
//
//  Created by Vikas Karambalkar on 25/10/14.
//
//

#import <UIKit/UIKit.h>
@class OLMRadioButton;
@protocol OLMRadioButtonDelegate<NSObject>
@required
-(void)OLMRadioButton:(OLMRadioButton*)radioButton didSelectOptionAtIndex:(NSInteger)index;
@end


@interface OLMRadioButton : UIView
{
    NSMutableArray *radioButtons;
    NSMutableDictionary *contollerInfo;
    NSArray *optionsArray;
}
@property (weak)id<OLMRadioButtonDelegate>delegate;
@property (nonatomic) NSInteger increamentedHeight;
@property (nonatomic) NSInteger currentSelectedIndex;
@property (nonatomic) NSInteger questionTypeId;
@property(nonatomic) NSInteger IsRequired;


-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId options:(NSArray*)optionsList IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)defaultValue delegate:(id)sender;

-(NSDictionary*)GetAnswerInfo;
-(NSDictionary*)GetHardcodedAnswer;

@end
