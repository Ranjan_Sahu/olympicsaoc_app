//
//  OLM
//  OLM
//
//  Created by Vikas Karambalkar on 30/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OLMLableSectionTitle : UIView
@property (nonatomic) NSInteger increamentedHeight;
-(id)initWithFrame:(CGRect)frame Text:(NSString*)text;
@property (nonatomic) NSInteger questionTypeId;
@end
