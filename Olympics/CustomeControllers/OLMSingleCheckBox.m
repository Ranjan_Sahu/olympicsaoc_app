//
//  UICheckBox.m
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMSingleCheckBox.h"

@implementation OLMSingleCheckBox
@synthesize increamentedHeight, delegate;

-(id)initWithFrame:(CGRect)frame questionId:(NSInteger)questionId fieldIdName:(NSString*)fieldIdName OptionTitle:(NSString*)OptionTitle isRequired:(BOOL)IsRequired defaultSelected:(BOOL)defaultSelected delegate:(id)sender
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];

        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        checkBoxButtons = [[NSMutableArray alloc]init];
        
        UIFont *lblfont = app_font13;
        if(IS_IPAD)
            lblfont = app_font24;
        
        CGSize stringsize = [OptionTitle sizeWithFont:lblfont constrainedToSize:CGSizeMake(self.frame.size.width, CGFLOAT_MAX)];
        
        _clickableButton = [[UIButton alloc]initWithFrame:CGRectMake( 0, increamentedHeight,self.frame.size.width - 10,stringsize.height)];
        [_clickableButton setTitleColor:color_gray forState:UIControlStateNormal];
        [_clickableButton.titleLabel setFont:lblfont];
        _clickableButton.titleLabel.numberOfLines = 50;
        increamentedHeight = increamentedHeight + stringsize.height + 10;
        [_clickableButton addTarget:self action:@selector(CheckBoxButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        _clickableButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _clickableButton.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
        [_clickableButton setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
        [_clickableButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
        [_clickableButton setTitle:OptionTitle forState:UIControlStateNormal];
        _clickableButton.titleEdgeInsets = UIEdgeInsetsMake(-3, 10, 0, 0);
        _clickableButton.tag = 0;
        [checkBoxButtons addObject:_clickableButton];
        [self addSubview:_clickableButton];
        
        if(defaultSelected)
        {
            [self setSelected];
        }
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:color_lightGray];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        
    }
    return self;
}

-(void) CheckBoxButtonClicked:(UIButton *) sender{
    if(!sender.isAccessibilityElement)
    {
        sender.isAccessibilityElement = TRUE;
        [sender setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
    }
    else
    {
        sender.isAccessibilityElement = FALSE;
        [sender setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
    }
    
    if([delegate respondsToSelector:@selector(OLMSingleCheckBox:didSelectOptionAtIndex:)])
    {
        [delegate OLMSingleCheckBox:self didSelectOptionAtIndex:[sender tag]];
    }
}

-(void)clearAll{
    for(int i=0;i<[checkBoxButtons count];i++){
        [[checkBoxButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"checkbox_off.png"] forState:UIControlStateNormal];
    }
}

-(NSDictionary*)GetAnswerInfo
{
    NSDictionary *response ;
    if([checkBoxButtons count]==0)
    {
        response = @{ [contollerInfo valueForKey:@"FieldIdName"]:@false };
    }
    else
    {
        UIButton *button = [checkBoxButtons objectAtIndex:0];
        if(button.isAccessibilityElement)
            response = @{ [contollerInfo valueForKey:@"FieldIdName"]:@true };
        else
            response = @{ [contollerInfo valueForKey:@"FieldIdName"]:@false };
    }
    return response;

}

-(void) setSelected{
    [self CheckBoxButtonClicked:[checkBoxButtons objectAtIndex:0]];
}


@end
