//
//  UICheckBox.h
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OLMCheckBox;
@protocol OLMCheckBoxDelegate<NSObject>
@required
-(void)OLMCheckBox:(OLMCheckBox*)checkBox didSelectOptionAtIndex:(NSInteger)index;
@end

@interface OLMCheckBox : UIView
{
    NSMutableArray *checkBoxButtons;
    NSMutableDictionary *contollerInfo;
    NSArray *optionsArray;
}

@property (weak)id<OLMCheckBoxDelegate>delegate;
@property(nonatomic,retain) UILabel *questionLbl;
@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property (nonatomic) NSInteger questionTypeId;
@property (strong, nonatomic) NSMutableArray *selectedOptionIdArray;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId options:(NSArray*)optionsList IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)defaultValue delegate:(id)sender;
-(NSDictionary*)GetAnswerInfo;

@end
