//
//  OLMLable.m
//  OLM
//
//  Created by Vikas Karambalkar on 30/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import "OLMLableSectionTitle.h"

@implementation OLMLableSectionTitle
@synthesize increamentedHeight;

-(id)initWithFrame:(CGRect)frame Text:(NSString*)text
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        increamentedHeight = 0;
        
        UILabel *textLable = [[UILabel alloc] init];
        textLable.textColor = color_white;
        if(IS_IPAD)
        {
            textLable.font = app_font_bold_24;
        }
        else
        {
            textLable.font = app_font_bold_16;
        }
        textLable.numberOfLines = 50;
        UIFont *font = textLable.font;
        CGRect textRect = [text boundingRectWithSize:CGSizeMake( self.frame.size.width-10, CGFLOAT_MAX)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:font}
                                                 context:nil];
        CGSize stringsize = textRect.size;
        
        textLable.frame = CGRectMake(5 , increamentedHeight, self.frame.size.width, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height;
        textLable.text = text;
        [self addSubview:textLable];
        
       
        increamentedHeight = increamentedHeight + 1 + 3;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        [self setBackgroundColor:[UIColor colorWithRed:0.1608 green:0.2941 blue:0.6471 alpha:1.0f]];
    }
    return self;
}


@end
