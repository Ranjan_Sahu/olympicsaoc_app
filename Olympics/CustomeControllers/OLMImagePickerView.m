//
//  OLMImagePickerView.m
//  Sales
//
//  Created by Vikas on 06/08/15.
//  Copyright (c) 2015 vikas. All rights reserved.
//

#import "OLMImagePickerView.h"

@implementation OLMImagePickerView
@synthesize increamentedHeight,delegate, imageView,imageForView,button;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId IsRequired:(NSInteger)IsRequired delegate:(id)sender
{
    if(IS_IPAD)
        frame = CGRectMake(frame.origin.x + 35, frame.origin.y, frame.size.width - 70, frame.size.height);
    self = [super initWithFrame:frame];
    if (self)
    {
        contollerInfo = [[NSMutableDictionary alloc] init];
        [contollerInfo setValue:question forKey:@"QuestionText"];
        [contollerInfo setValue:fieldIdName forKey:@"FieldIdName"];
        [contollerInfo setValue:[NSNumber numberWithInteger:questionId] forKey:@"QuestionId"];
        
        delegate = sender;
        self.IsRequired = IsRequired;
        increamentedHeight = 0;
        
        UILabel *questionLbl = [[UILabel alloc] init];
        if(IS_IPAD)
        {
            questionLbl.font = app_font_bold_18;
        }
        else
        {
            questionLbl.font = app_font_bold_10;
        }
        questionLbl.textColor = color_blueFont;
        questionLbl.numberOfLines = 50;
        UIFont *font = questionLbl.font;
        CGRect textRect = [question boundingRectWithSize:CGSizeMake( self.frame.size.width, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
        
        CGSize stringsize = textRect.size;
        questionLbl.frame = CGRectMake(5 , increamentedHeight, self.frame.size.width - 10, stringsize.height);
        increamentedHeight = increamentedHeight + stringsize.height + 5;
        questionLbl.text = question;
        
        [self addSubview:questionLbl];
        
        if(IS_IPAD)
        {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, increamentedHeight + 10, 60, 48)];
        }
        else
        {
            imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, increamentedHeight + 10, 42, 36)];
        }
        imageView.image = [UIImage imageNamed: @"photo_box"];
        [imageView setContentMode:UIViewContentModeScaleAspectFill];
        [imageView setClipsToBounds:YES];
        [self addSubview: imageView];
        
        CGRect imageframe = imageView.frame;
        
        UILabel *imageLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageframe.origin.x + imageframe.size.width, imageframe.origin.y, imageView.frame.size.width * 3, imageframe.size.height)];
        imageLabel.text = @"Upload A Photo";
        imageLabel.textAlignment = NSTextAlignmentCenter;
        imageLabel.font = app_font_bold_13;
        imageLabel.backgroundColor = [UIColor colorWithRed:220.0/255.0 green:220.0/255.0 blue:220.0/255.0 alpha:0.9];
        imageLabel.textColor = [UIColor colorWithRed:35.0/255.0 green:53.0/255.0 blue:51.0/255.0 alpha:0.9];
        [self addSubview:imageLabel];

        
        button = [[UIButton alloc] initWithFrame:CGRectMake(imageframe.origin.x, imageframe.origin.y, imageView.frame.size.width + imageLabel.frame.size.width, imageframe.size.height)];
        [button addTarget:self action:@selector(didSelectCameraButton:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundColor:[UIColor clearColor]];
        [self addSubview:button];
        [self bringSubviewToFront:button];
        
        increamentedHeight = increamentedHeight +  (imageView.frame.size.height+25) + 5;
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, increamentedHeight, self.frame.size.width, 1)];
        [line setBackgroundColor:color_lightGray];
        [self addSubview:line];
        increamentedHeight = increamentedHeight + 1 + 5;
        
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, increamentedHeight);
        
        if(IsRequired == 1)
        {
            UILabel *mandetoryLabel = [[UILabel alloc] init];
            mandetoryLabel.textAlignment = NSTextAlignmentCenter;
            if(IS_IPAD)
            {
                mandetoryLabel.frame = CGRectMake(questionLbl.frame.origin.x - 47 + questionLbl.frame.size.width, questionLbl.frame.origin.y, 50, 50);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:40];
                
            }
            else
            {
                mandetoryLabel.frame = CGRectMake(questionLbl.frame.origin.x - 25 + questionLbl.frame.size.width, questionLbl.frame.origin.y, 30, 30);
                mandetoryLabel.font = [UIFont fontWithName:@"icomoon" size:25];
                
            }
            mandetoryLabel.textColor = [UIColor blackColor];
            mandetoryLabel.text = @"\ue923";
            [self addSubview:mandetoryLabel];
        }
    }
    return self;
}


-(NSDictionary*)GetAnswerInfo
{
    NSMutableArray *AnswersList = [[NSMutableArray alloc] init];
    
    NSMutableDictionary *answer = [[NSMutableDictionary alloc] init];
    [answer setValue:nil forKey:@"AnswerId"];
    
    if([self.imageView.image isEqual:[UIImage imageNamed:@"photo_box"]])
    {
        //        [answer setValue:nil forKey:@"AnswerText"];
    }
    else
    {
        NSData *imageData = UIImagePNGRepresentation(self.imageView.image);
        NSString *imageString = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        [answer setObject:imageString forKey:@"AnswerText"];
    }
    
    [AnswersList addObject:answer];
    [contollerInfo setObject:AnswersList forKey:@"AnswerList"];
    
    return contollerInfo;
}

#pragma mark -
#pragma mark -UIImagePickerController Delegate Methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
    UIImage *imageToSave;
    
    if([mediaType isEqualToString:(NSString*)kUTTypeImage]) {
        UIImage *photoTaken =  info[UIImagePickerControllerOriginalImage];
        if (picker.sourceType == UIImagePickerControllerSourceTypeCamera || picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary)
        {
            imageToSave = [self imageWithImage:photoTaken scaledToSize:CGSizeMake(photoTaken.size.width/10, photoTaken.size.height/10)];
        }
    }
    self.imageView.image = imageToSave;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [delegate OLMImagePickerDidDismissWithImage:imageToSave forImageView:self.imageView];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/*
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = TRUE;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unsupported!"
                                                                           message:@"This device does not have a Photo liabrary."
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:(UIAlertActionStyleCancel)
                                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                                            
                                                                        }];
            [alert addAction:alert_cancel_action];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
            
            //UIAlertView *alert =[ [UIAlertView alloc] initWithTitle:@"Unsupported!" message:@"This device does not have a Photo liabrary." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //[alert show];
        }
        
    }
    else if (buttonIndex == 1)
    {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = TRUE;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unsupported!"
                                                                           message:@"This device does not have a camera."
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:(UIAlertActionStyleCancel)
                                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                                            
                                                                        }];
            [alert addAction:alert_cancel_action];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];

            //UIAlertView *alert =[ [UIAlertView alloc] initWithTitle:@"Unsupported!" message:@"This device does not have a camera." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //[alert show];
        }
        
    }
    
}
*/

-(void)didSelectCameraButton:(id)sender {
    
//    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Choose" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//        
//    }]];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
//        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //picker.allowsEditing = TRUE;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unsupported!"
                                                                           message:@"This device does not have a Photo liabrary."
                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
            
            UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Ok"
                                                                          style:(UIAlertActionStyleCancel)
                                                                        handler:^(UIAlertAction * _Nonnull action) {
                                                                            
                                                                        }];
            [alert addAction:alert_cancel_action];
            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
            
            //UIAlertView *alert =[ [UIAlertView alloc] initWithTitle:@"Unsupported!" message:@"This device does not have a Photo liabrary." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //[alert show];
        }

//    }]];
//    
//    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//        
//        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//        {
//            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//            picker.delegate = self;
//            picker.allowsEditing = TRUE;
//            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//            [self.window.rootViewController presentViewController:picker animated:YES completion:NULL];
//        }
//        else
//        {
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unsupported!"
//                                                                           message:@"This device does not have a camera."
//                                                                    preferredStyle:(UIAlertControllerStyleAlert)];
//            
//            UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Ok"
//                                                                          style:(UIAlertActionStyleCancel)
//                                                                        handler:^(UIAlertAction * _Nonnull action) {
//                                                                            
//                                                                        }];
//            [alert addAction:alert_cancel_action];
//            [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
//            
//            //UIAlertView *alert =[ [UIAlertView alloc] initWithTitle:@"Unsupported!" message:@"This device does not have a camera." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//            //[alert show];
//        }
//        
//    }]];
//    
//    [self.window.rootViewController presentViewController:actionSheet animated:YES completion:nil];
//    
    //UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Photo Library" otherButtonTitles:@"Camera", nil];
    //[actionSheet showInView:[APP_DELEGATE window].rootViewController.view];
    
}

@end
