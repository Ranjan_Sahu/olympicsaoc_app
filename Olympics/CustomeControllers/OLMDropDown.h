//
//  UIDropdown.h
//  OLM
//
//  Created by Vikas Karambalkar on 29/10/14.
//  Copyright (c) 2014 vikas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DesabledPasteOption_UITextField.h"
@class OLMDropDown;
@protocol OLMDropDownDelegate<NSObject>
-(void)OLMDropdownDidStartEditing:(UITextField*)textField;
-(void)OLMDropdownDidEndEditing:(UITextField*)textField;
@required
@end


@interface OLMDropDown : UIView<UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate>
{
    NSArray *optionsList;
    UIPickerView *optionsPicker;
    NSMutableDictionary *contollerInfo;
}

@property (nonatomic) NSInteger increamentedHeight;
@property(nonatomic) NSInteger IsRequired;
@property(nonatomic,retain) DesabledPasteOption_UITextField *optionTextfield;
@property(nonatomic,retain) UILabel *questionLbl;
@property (weak)id<OLMDropDownDelegate>delegate;
@property (nonatomic) NSInteger currentSelectedIndex;
@property (nonatomic) NSInteger questionTypeId;

-(id)initWithFrame:(CGRect)frame Question:(NSString*)question fieldIdName:(NSString*)fieldIdName questionId:(NSInteger)questionId Options:(NSArray*)options IsRequired:(NSInteger)IsRequired defaultValue:(NSInteger)value delegate:(id)sender;
-(NSDictionary*)GetAnswerInfo;
-(NSDictionary*)GetHardcodedAnswer;
@end
